package com.matt1.thenewboston;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;

/**
 * Created by Matthew on 12/20/2014.
 */
public class CalcMenu extends Activity implements View.OnClickListener {

    TextView question, calctitle;
    public Class ourClass;
    Trivia[] questions = new Trivia[501];
    Button prayer, cooking, woodcutting, firemaking, fishing, magic, crafting, mining, smithing, herblore, thieving, agility, farming, runecrafting, hunter, construction;


    ImageView correct;

    Trivia currentQuestion;
    AlertDialog.Builder alert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calcmenu);

        calctitle = (TextView) findViewById(R.id.calcmenutitle);
        prayer = (Button) findViewById(R.id.mmPrayer);
        cooking = (Button) findViewById(R.id.mmCooking);
        woodcutting = (Button) findViewById(R.id.mmWoodcutting);
        firemaking = (Button) findViewById(R.id.mmFiremaking);
        fishing = (Button) findViewById(R.id.mmFishing);
        magic = (Button) findViewById(R.id.mmMagic);
        crafting = (Button) findViewById(R.id.mmCrafting);
        mining = (Button) findViewById(R.id.mmMining);
        smithing = (Button) findViewById(R.id.mmSmithing);
        herblore = (Button) findViewById(R.id.mmHerblore);
        thieving = (Button) findViewById(R.id.mmThieving);
        agility = (Button) findViewById(R.id.mmAgility);
        farming = (Button) findViewById(R.id.mmFarming);
        runecrafting = (Button) findViewById(R.id.mmRunecrafting);
        hunter = (Button) findViewById(R.id.mmHunter);
        construction = (Button) findViewById(R.id.mmConstruction);

        prayer.setOnClickListener(this);
        cooking.setOnClickListener(this);
        woodcutting.setOnClickListener(this);
        firemaking.setOnClickListener(this);
        fishing.setOnClickListener(this);
        magic.setOnClickListener(this);
        crafting.setOnClickListener(this);
        mining.setOnClickListener(this);
        smithing.setOnClickListener(this);
        herblore.setOnClickListener(this);
        thieving.setOnClickListener(this);
        agility.setOnClickListener(this);
        farming.setOnClickListener(this);
        runecrafting.setOnClickListener(this);
        hunter.setOnClickListener(this);
        construction.setOnClickListener(this);

        Typeface myTypeface = Typeface.createFromAsset(getAssets(), "fonts/runescape_uf.ttf");

        calctitle.setTypeface(myTypeface);
        prayer.setTypeface(myTypeface);
        cooking.setTypeface(myTypeface);
        woodcutting.setTypeface(myTypeface);
        firemaking.setTypeface(myTypeface);
        fishing.setTypeface(myTypeface);
        magic.setTypeface(myTypeface);
        crafting.setTypeface(myTypeface);
        mining.setTypeface(myTypeface);
        smithing.setTypeface(myTypeface);
        herblore.setTypeface(myTypeface);
        thieving.setTypeface(myTypeface);
        agility.setTypeface(myTypeface);
        farming.setTypeface(myTypeface);
        runecrafting.setTypeface(myTypeface);
        hunter.setTypeface(myTypeface);
        construction.setTypeface(myTypeface);


    }
    @Override
    public void onClick(View v) {


        switch (v.getId()) {
            case R.id.mmPrayer:


                try {
                    ourClass = Class.forName("com.matt1.thenewboston.Prayer");
                    Intent ourIntent = new Intent(CalcMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;

            case R.id.mmCooking:


                try {
                    ourClass = Class.forName("com.matt1.thenewboston.Cooking");
                    Intent ourIntent = new Intent(CalcMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;

            case R.id.mmWoodcutting:


                try {
                    ourClass = Class.forName("com.matt1.thenewboston.Woodcutting");
                    Intent ourIntent = new Intent(CalcMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;

            case R.id.mmFiremaking:


                try {
                    ourClass = Class.forName("com.matt1.thenewboston.Firemaking");
                    Intent ourIntent = new Intent(CalcMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;

            case R.id.mmFishing:


                try {
                    ourClass = Class.forName("com.matt1.thenewboston.Fishing");
                    Intent ourIntent = new Intent(CalcMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;

            case R.id.mmMagic:


                try {
                    ourClass = Class.forName("com.matt1.thenewboston.Magic");
                    Intent ourIntent = new Intent(CalcMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;



            case R.id.mmCrafting:


                try {
                    ourClass = Class.forName("com.matt1.thenewboston.Crafting");
                    Intent ourIntent = new Intent(CalcMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;

            case R.id.mmMining:


                try {
                    ourClass = Class.forName("com.matt1.thenewboston.Mining");
                    Intent ourIntent = new Intent(CalcMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;

            case R.id.mmSmithing:


                try {
                    ourClass = Class.forName("com.matt1.thenewboston.Smithing");
                    Intent ourIntent = new Intent(CalcMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;

            case R.id.mmHerblore:


                try {
                    ourClass = Class.forName("com.matt1.thenewboston.Herblore");
                    Intent ourIntent = new Intent(CalcMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;

            case R.id.mmThieving:


                try {
                    ourClass = Class.forName("com.matt1.thenewboston.Thieving");
                    Intent ourIntent = new Intent(CalcMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;

            case R.id.mmAgility:


                try {
                    ourClass = Class.forName("com.matt1.thenewboston.Agility");
                    Intent ourIntent = new Intent(CalcMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;

            case R.id.mmFarming:


                try {
                    ourClass = Class.forName("com.matt1.thenewboston.Farming");
                    Intent ourIntent = new Intent(CalcMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;

            case R.id.mmRunecrafting:


                try {
                    ourClass = Class.forName("com.matt1.thenewboston.Runecrafting");
                    Intent ourIntent = new Intent(CalcMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;

            case R.id.mmHunter:


                try {
                    ourClass = Class.forName("com.matt1.thenewboston.Hunter");
                    Intent ourIntent = new Intent(CalcMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;

            case R.id.mmConstruction:


                try {
                    ourClass = Class.forName("com.matt1.thenewboston.Construction");
                    Intent ourIntent = new Intent(CalcMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;

        }





    }





}