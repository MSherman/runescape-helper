package com.matt1.thenewboston;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.view.View;
import android.view.inputmethod.InputMethodManager;



public class RevampedScores extends Activity implements View.OnClickListener{

    public TextView skillLvl[] = new TextView[24];
    public TextView skillRank[] = new TextView[24];
    public TextView skillXP[] = new TextView[24];
    public TextProgressBar progress[] =  new TextProgressBar[24];
    String name;
    public EditText username;
    public ImageButton search;
    String url =  "http://services.runescape.com/m=hiscore_oldschool/hiscorepersonal.ws?user1=";
    private AnimationDrawable myAnimationDrawable1;
    private ImageView runes;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        super.onCreate(savedInstanceState);
        setContentView(R.layout.revamphs);

        String filename = "mysecondfile";

        File myDir = getFilesDir();

        try {
            File secondInputFile = new File(myDir + "/text/", filename);
            InputStream secondInputStream = new BufferedInputStream(new FileInputStream(secondInputFile));
            BufferedReader r = new BufferedReader(new InputStreamReader(secondInputStream));
            StringBuilder total = new StringBuilder();
            String line;
            //      Log.e("does this work", "please");
            while ((line = r.readLine()) != null) {
                total.append(line);
                //           Log.e("appeneded", "nice job");
            }
//            Log.e("this is the Line", line);
            r.close();
            secondInputStream.close();
            name = total.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }


        runes = (ImageView) findViewById(R.id.incrementingBoxView);

        runes.setVisibility(View.GONE);




        search =  (ImageButton) findViewById(R.id.ibSearch4);
        username = (EditText) findViewById(R.id.h2Username);

        username.setInputType(android.text.InputType.TYPE_CLASS_TEXT | android.text.InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

        search.setOnClickListener(this);


        username.setText(name);

        skillLvl[0] = (TextView) findViewById(R.id.totallvl);
        skillLvl[1] = (TextView) findViewById(R.id.attacklvl);
        skillLvl[2] = (TextView) findViewById(R.id.defencelvl);
        skillLvl[3] = (TextView) findViewById(R.id.strengthlvl);
        skillLvl[4] = (TextView) findViewById(R.id.hitpointslvl);
        skillLvl[5] = (TextView) findViewById(R.id.rangedlvl);
        skillLvl[6] = (TextView) findViewById(R.id.prayerlvl);
        skillLvl[7] = (TextView) findViewById(R.id.magiclvl);
        skillLvl[8] = (TextView) findViewById(R.id.cookinglvl);
        skillLvl[9] = (TextView) findViewById(R.id.woodcuttinglvl);
        skillLvl[10] = (TextView) findViewById(R.id.fletchinglvl);
        skillLvl[11] = (TextView) findViewById(R.id.fishinglvl);
        skillLvl[12] = (TextView) findViewById(R.id.firemakinglvl);
        skillLvl[13] = (TextView) findViewById(R.id.craftinglvl);
        skillLvl[14] = (TextView) findViewById(R.id.smithinglvl);
        skillLvl[15] = (TextView) findViewById(R.id.mininglvl);
        skillLvl[16] = (TextView) findViewById(R.id.herblorelvl);
        skillLvl[17] = (TextView) findViewById(R.id.agilitylvl);
        skillLvl[18] = (TextView) findViewById(R.id.thievinglvl);
        skillLvl[19] = (TextView) findViewById(R.id.slayerlvl);
        skillLvl[20] = (TextView) findViewById(R.id.farminglvl);
        skillLvl[21] = (TextView) findViewById(R.id.runecraftlvl);
        skillLvl[22] = (TextView) findViewById(R.id.hunterlvl);
        skillLvl[23] = (TextView) findViewById(R.id.constructionlvl);





    }

    public void incrementalHorizontalLoading() {
        myAnimationDrawable1 = (AnimationDrawable) runes.getDrawable();
        myAnimationDrawable1.start();

    }


    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);


        name = username.getText().toString();

        url = "http://services.runescape.com/m=hiscore_oldschool/index_lite.ws?player=" + name;
        new Title().execute();

        runes.setVisibility(View.VISIBLE);
        incrementalHorizontalLoading();
        //test.setText(url);



    }



    class Title extends AsyncTask<Void, Void, Void> {
        String title;
        String content;

        @Override
        protected Void doInBackground(Void... params) {
            try {
                // Connect to the web site
                Document document = Jsoup.connect(url).ignoreHttpErrors(true).get();
                // Get the html document title
                if(document.hasText())
                    content = document.text();
                // title = document.title();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // Set title into TextView

            String delims = "[ ,]";


            int lvl = 1;
            int rank = 0;
            int xp = 2;
            int x = 0;

            int current = 0;
            double left = 0;
            double y = 0;
            int percent = 0;

            if (content.length() > 22) {

                String[] data = content.split(delims);
                DecimalFormat formatter = new DecimalFormat("#,###,###");
                String commas = formatter.format(100000);

                while(x<24)
                {




                    skillLvl[x].setText(data[lvl]);




                    lvl+=3;
                    rank+=3;
                    xp+=3;
                    x++;


                }

                //calculateXP();
            }

            runes.setVisibility(View.GONE);
            myAnimationDrawable1.stop();

        }
    }
}



