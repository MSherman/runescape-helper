package com.matt1.thenewboston;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;



public class Anagram extends Activity implements AdapterView.OnItemSelectedListener {

    ArrayList<String> clue = new ArrayList<String>();
    ArrayList<AnagramBuilder> sorted = new ArrayList<AnagramBuilder>();
    private static final String TAG = PriceGuide.class.getSimpleName();

    String url = "http://2007.runescape.wikia.com/wiki/Treasure_Trails/Guide/Cryptic_clues";
    ListView anagramView;
    Spinner letters;

    AnagramBuilder[] anas = new AnagramBuilder[59];

    TextView anagramtitle;


    BaseAdapter2 adapter2;

    ImageButton searchClue;
    String starts;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.anagram);

        letters = (Spinner) findViewById(R.id.spinner);
// Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.letters_array, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        letters.setAdapter(adapter);

        anagramtitle = (TextView) findViewById(R.id.anagramtitle);

        Typeface myTypeface = Typeface.createFromAsset(getAssets(), "fonts/runescape_uf.ttf");


        anagramtitle.setTypeface(myTypeface);

        anas[0] = new AnagramBuilder("A Baker", "Bareak", "Varrock square (fur trader)", "5");
        anas[1] =  new AnagramBuilder("A Basic Anti Pot", "Captain Tobias", "Port Sarim", "5");
        anas[2] =  new AnagramBuilder("A Zen She", "Zenesha", " \tThe Platebody shop on the Southern edge of Ardougne centre square", "Puzzle Box");
        anas[3] =  new AnagramBuilder("Ace Match Elm", "Cam The Camel", "North of the glider in Al Kharid, outside of the town. Camulet not required ", "Puzzle Box");
        anas[4] =  new AnagramBuilder("Aha Jar", "Jaraah", "The hospital at the Duel Arena", "None");
        anas[5] =  new AnagramBuilder("An Paint Tonic", "Captain Ninto", "In the bar under White Wolf Mountain, requiring the quest Fishing Contest", "Puzzle box");
        anas[6] =  new AnagramBuilder("Arc O Line", "Caroline", "North Witchaven next to the row boat. After Sea Slug Quest is found wandering the coast.", "");
        anas[7] =  new AnagramBuilder("Are Col", "Oracle", "The peak of Ice Mountain West of Edgeville", "48");
        anas[8] =  new AnagramBuilder("Arr! So I am a crust, and?", "Ramara du Croissant", "In the forge in Piscatoris Fishing Colony, requiring the quest Swan Song. (north from the fairy ring code AKQ).", "Puzzle Box");
        anas[9] =  new AnagramBuilder("A Bas", "Saba", " \tFound in the cave on the path towards the Death Plateau west of Burthorpe.", "None");
        anas[10] =  new AnagramBuilder("Bail Trims", "Brimstail", "In the cave West of the South bank in Gnome Stronghold", "None");
        anas[11] =  new AnagramBuilder("By Look", "Bolkoy", "In the Tree Gnome Village general store(upstairs)", "13");
        anas[12] =  new AnagramBuilder("Career In Moon", "Oneiromancer", "Astral altar on Lunar Isle", "25");
        anas[13] =  new AnagramBuilder("C On Game Hoc", "Gnome Coach", "Walking around outside the Gnome Ball course, West of the Grand Tree", "6");
        anas[14] =  new AnagramBuilder("Cool Nerd", "Old Crone", "East of the Slayer Tower", "619");
        anas[15] =  new AnagramBuilder("Copper Ore Crypts", "Prospector Percy", "Motherlode Mine , you'll need a pickaxe to reach him.", "12");
        anas[16] =  new AnagramBuilder("Do Say More", "Doomsayer", "East of Lumbridge castle, near the Lumbridge guide", "95");
        anas[17] =  new AnagramBuilder("Dragons Lament", "Strange Old Man", "Gravedigger at Barrows", "40");
        anas[18] =  new AnagramBuilder("Dr Hitman", "Mandrith", "Wilderness resource area", "28");
        anas[19] =  new AnagramBuilder("Dt Run B", "Brundt the Chieftain", "Rellekka, inside the main hall", "4");
        anas[20] =  new AnagramBuilder("Eek Zero Op", "Zookeeper", "Ardougne Zoo", "40(41 if Eagles Peak is done)");
        anas[21] =  new AnagramBuilder("El Ow", "Lowe", "Varrock archery shop", "None");
        anas[22] =  new AnagramBuilder("Err Cure It", "Recruiter", "West Ardougne centre square", "20");
        anas[23] =  new AnagramBuilder("Goblim Kern", "King Bolren", "Tree Gnome Village, next to the Spirit Tree", "None");
        anas[24] =  new AnagramBuilder("Got A Boy", "Gabooty", "Center of Tai Bwo Wannai", "11");
        anas[25] =  new AnagramBuilder("Gulag Run", "Ulgug Nar", "To the west of Jiggig, just South of the Castle Wars lobby", "Puzzle box");
        anas[26] =  new AnagramBuilder("Halt Us", "Luthas", "The owner of the banana plantation on Musa Point (Karamja).", "33(or None)");
        anas[27] =  new AnagramBuilder("He Do Pose. It Is Cultrrl, Mk?", "Riki the sculptor's model", "East side of Keldagrim, south of the kebab seller.", "Puzzle Box");
        anas[28] =  new AnagramBuilder("Heoric", "Eohric", "Burthorpe Castle top floor", "36");
        anas[29] =  new AnagramBuilder("Icy Fe", "Fycie", " \tRantz's cave, South-east of Gu'Tanoth and Far Eastern portion of Feldip Hills (near the coast).", "None");
        anas[30] =  new AnagramBuilder("I Eat Its Chart Hints Do U", "Shiratti the Custodian", "North of the fountain in Nardah","Puzzle Box");
        anas[31] =  new AnagramBuilder("I Even", "Nieve", "The slayer master in Gnome Stronghold", "2");
        anas[32] =  new AnagramBuilder("I Faffy Run", "Fairy Nuff", "Before Fairy Tale II - Cure a Queen she can be found just north of the bank in Zanaris. After Fairy Tale II, however, players must get the certificate from the potion shelf (located in the room north of the fairy bank) and use the fairy rings in this order AIR, DLR, DJQ, AJS.", "Puzzle Box");
        anas[33] =  new AnagramBuilder("Kay Sir", "Sir Kay", "The courtyard in Camelot Castle", "6");
        anas[34] =  new AnagramBuilder("Leakay", "Kaylee", "Rising Sun Inn in Falador", "18");
        anas[35] =  new AnagramBuilder("Land Doomd", "Odd Old Man", "The Limestone mine northeast of Varrock", "Puzzle Box");
        anas[36] =  new AnagramBuilder("Lark In Dog", "King Roald", "Ground floor of Varrock Castle", "24");
        anas[37] =  new AnagramBuilder("Majors Lava Bads Air", "Ambassador Alvijar", "North-east Dorgesh-Kaan, upper floor, just south of the house with the quest symbol", "2505");
        anas[38] =  new AnagramBuilder("Me Am The Calc", "Cam the Camel", "Just outside the gates of the Duel Arena, Camulet not required ", "Puzzle Box");
        anas[39] =  new AnagramBuilder("Machete Clam", "Cam the Camel", "Just outside the gates of the Duel Arena, Camulet not required ", "6");
        anas[40] =  new AnagramBuilder("Me If", "Femi", "Just outside the gates of the Tree Gnome Stronghold", "None");
        anas[41] =  new AnagramBuilder("Motherboard", "Brother Omad", "Monastery south of Ardougne", "129");
        anas[42] =  new AnagramBuilder("No Owner", "Oronwen", "Lletya Seamstress shop in Lletya", "20");
        anas[43] =  new AnagramBuilder("Nod Med", "Edmond", "Behind the most northwestern house in East Ardougne", "3");
        anas[44] =  new AnagramBuilder("O Birdz A Zany En Pc", "Cap'n Izzy no Beard", "Entrance of the Brimhaven Agility Arena", "33");
        anas[45] =  new AnagramBuilder("Ok Co", "Cook", "Ground floor of Lumbridge Castle ", "9");
        anas[46] =  new AnagramBuilder("Or Zinc Fumes Ward", "Wizard Frumscone", "Downstairs in the Wizards' Guild (requires 66 Magic to enter, boosted)", "Puzzle Box");
        anas[47] =  new AnagramBuilder("Peaty Pert", "Party Pete", "Falador Party Room", "None");
        anas[48] =  new AnagramBuilder("Profs Lose Wrong Pie", "Professor Onglewip", "Ground floor of the Wizards Tower south of Draynor", "Puzzle Box");
        anas[49] =  new AnagramBuilder("R Ak Mi", "Karim", "Al Kharid Kebab shop just south of the furnace", "5");
        anas[50] =  new AnagramBuilder("Rat Mat Within", "Martin Thwait", "Rogues' Den", "2");
        anas[51] =  new AnagramBuilder("Red Art Tans ", "Trader Stan", "Port Sarim Charter ship ", "Puzzle Box");
        anas[52] =  new AnagramBuilder("Ratai", "Taria", "Rimmington bush patch", "7");
        anas[53] =  new AnagramBuilder("Sequin Dirge", "Queen Sigrid", "Throne room of Etceteria Castle", "Puzle Box");
        anas[54] =  new AnagramBuilder("Slide Woman", "Wise Old Man", "Draynor Village", "28");
        anas[55] =  new AnagramBuilder("Snah", "Hans", "Lumbridge Castle courtyard", "None");
        anas[56] =  new AnagramBuilder("Them Cal Came", "Cam the Camel", "Just outside the gates of the Duel Arena, Camulet not required", "Puzzle box");
        anas[57] =  new AnagramBuilder("Thickno", "Hickton ", "Hickton's Archery Emporium in Catherby. Second house east of the bank.", "2");
        anas[58] =  new AnagramBuilder("Unleash Night Mist", "Sigli the Huntsman", "Rellekka", "302 ");





        anagramView = (ListView) findViewById(R.id.anagramListview);

        for(int x = 0; x<anas.length; x++)
        {
            if(anas[x].getQuestion().startsWith("A"))
                sorted.add(anas[x]);


        }


        letters.setOnItemSelectedListener(this);


        adapter2 = new BaseAdapter2(Anagram.this, sorted);
        anagramView.setAdapter(adapter2);




    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        sorted.clear();

        for(int x = 0; x<anas.length; x++)
        {
            if(anas[x].getQuestion().startsWith(letters.getSelectedItem().toString()))
                sorted.add(anas[x]);


        }


        anagramView.setAdapter(null);


        adapter2 = new BaseAdapter2(Anagram.this, sorted);
        anagramView.setAdapter(adapter2);




    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


    public class BaseAdapter2 extends BaseAdapter {

        private Activity activity;
        // private ArrayList&lt;HashMap&lt;String, String&gt;&gt; data;
        private ArrayList clues, answers;
        private ArrayList<AnagramBuilder> anagrams;




        private final String TAG = BaseAdapter2.class.getSimpleName();
        private LayoutInflater inflater = null;
        String letter;

        int x = 0;


        public BaseAdapter2(Activity a,  ArrayList<AnagramBuilder> b) {
            activity = a;

            this.anagrams = b;


            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }


        public int getCount() {
            return anagrams.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, final ViewGroup parent) {
            View vi = convertView;
            if (convertView == null)
                vi = inflater.inflate(R.layout.anagram_item, null);





                TextView title = (TextView) vi.findViewById(R.id.anagram); // title
                final String item = anagrams.get(position).getQuestion();
                title.setText(item);


                TextView title1 = (TextView) vi.findViewById(R.id.anagramsolved); // notice
                String average = anagrams.get(position).getAnswer();
                title1.setText(average);

                TextView title2 = (TextView) vi.findViewById(R.id.anagramlocation); // title
                final String loc = anagrams.get(position).getLocation();
                title2.setText("Location: " + loc);


                TextView title22 = (TextView) vi.findViewById(R.id.answers); // notice
                String challenge = anagrams.get(position).getChallenge();
                title22.setText(challenge);


                return vi;


        }


    }









}