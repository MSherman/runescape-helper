package com.matt1.thenewboston;

import android.app.AlertDialog;
import android.app.DialogFragment;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.app.Dialog;
import android.app.Activity;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Menu extends ListActivity{


    String rsname = "";
    String classes[] = { "Hiscores", "Firemaking", "Woodcutting", "Hiscores2", "TriviaGame", "PriceGuide", "CrypticClue", "MainMenu", "Anagram", "MapClue", "Loading",
    "QuestGuides", "Map", "XPDrop", "RuneAnim", "Tweets", "Alerts"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setListAdapter(new ArrayAdapter<String>(Menu.this, android.R.layout.simple_list_item_1, classes));
        LoginDialog newFragment = new LoginDialog();

        String filename = "mysecondfile";

        File myDir = getFilesDir();



        try {
            File secondInputFile = new File(myDir + "/text/", filename);
            InputStream secondInputStream = new BufferedInputStream(new FileInputStream(secondInputFile));
            BufferedReader r = new BufferedReader(new InputStreamReader(secondInputStream));
            StringBuilder total = new StringBuilder();
            String line;
            while ((line = r.readLine()) != null) {
                total.append(line);
            }
            r.close();
            secondInputStream.close();
            rsname = total.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }



        if(rsname.length()<1)
        newFragment.show(getFragmentManager(), "Login");

    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        // TODO Auto-generated method stub
        super.onListItemClick(l, v, position, id);

        String cheese = classes[position];


        Class ourClass;
        try {
            ourClass = Class.forName("com.matt1.thenewboston." + cheese);
            Intent ourIntent = new Intent(Menu.this, ourClass);
            startActivity(ourIntent);
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        //DialogFragment login = new LoginDialog();
        // login.confirmLogin();

    }

    public class LoginDialog extends DialogFragment {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            // Get the layout inflater
            LayoutInflater inflater = getActivity().getLayoutInflater();

            // Inflate and set the layout for the dialog
            // Pass null as the parent view because its going in the dialog layout
            final View view = inflater.inflate(R.layout.dialog_signin, null);
            final EditText RSN = (EditText) view.findViewById(R.id.rsn);
            builder.setView(view)
                    // Add action buttons
                    .setPositiveButton("Save RSN", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {

                            String filename = "mysecondfile";
                            String outputString = RSN.getText().toString();
                            File myDir = getFilesDir();

                            try {
                                File secondFile = new File(myDir + "/text/", filename);
                                if (secondFile.getParentFile().mkdirs()) {
                                    secondFile.createNewFile();
                                    FileOutputStream fos = new FileOutputStream(secondFile);

                                    fos.write(outputString.getBytes());

                                    fos.flush();
                                    fos.close();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                            // sign in the user ...
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            LoginDialog.this.getDialog().cancel();
                        }
                    });
            return builder.create();
        }

        public void confirmLogin() {
            LoginDialog newFragment = new LoginDialog();
            newFragment.show(getFragmentManager(), "Login");
        }
    }




}



