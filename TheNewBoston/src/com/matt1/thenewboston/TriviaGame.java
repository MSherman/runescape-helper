package com.matt1.thenewboston;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Matthew on 12/20/2014.
 */
public class TriviaGame extends Activity implements View.OnClickListener {

    TextView question;
    ArrayList<Trivia> questions = new ArrayList<Trivia>();
    Button a, b, c, d;
    int x = 0;
    private ObjectAnimator waveThreeAnimator;
    ImageView correct;
    TextView xpdrop;
    Trivia currentQuestion;
    AlertDialog.Builder alert;
    int totalQ =0;
    int totalCorrect = 0;
    int longestStreak =0;
    int currentStreak =0;
    int points =0;
    boolean isStreak = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trivia);

        question = (TextView) findViewById(R.id.question);
        a = (Button) findViewById(R.id.answer1);
        b = (Button) findViewById(R.id.answer2);
        c = (Button) findViewById(R.id.answer3);
        d = (Button) findViewById(R.id.answer4);

        a.setOnClickListener(this);
        b.setOnClickListener(this);
        c.setOnClickListener(this);
        d.setOnClickListener(this);


        xpdrop = (TextView) findViewById(R.id.xpdroplet);


        Typeface myTypeface = Typeface.createFromAsset(getAssets(), "fonts/runescape_uf.ttf");

        xpdrop.setTypeface(myTypeface);
        a.setTypeface(myTypeface);
        b.setTypeface(myTypeface);
        c.setTypeface(myTypeface);
        d.setTypeface(myTypeface);
        question.setTypeface(myTypeface);

        correct = (ImageView) findViewById(R.id.correct2);


        questions.add(new Trivia("What level can \n you fish sharks?", "70", "74", "80", "76", "76"));
        questions.add( new Trivia("What level can \n you fish Lobster?", "40", "45", "35", "50", "40"));
        questions.add(new Trivia("What level can \n you fish Dark Crab?", "85", "80", "90", "84", "85"));
        questions.add(new Trivia("How much XP do you get \n" +
                " for catching a Monkfish?", "100", "120", "110", "90", "120"));
        questions.add(new Trivia("How much XP do you get \n" +
                " for cutting a Yew tree?", "150", "167.5", "185", "175", "175"));
        questions.add(new Trivia("How much XP do you get \n" +
                " for cutting a Teak tree?", "85", "75", "80", "65", "85"));
        questions.add(new Trivia("How much XP do you get \n" +
                " for cutting a Maple tree?", "112.5", "100", "97.5", "90", "100"));
        questions.add(new Trivia("How much XP do you get \n" +
                " for cutting a Normal tree?", "20", "25", "40", "35", "25"));
        questions.add(new Trivia("How much XP do you get \n for catching a Black Chinchompa?", "315", "250", "300", "325", "315"));
        questions.add(new Trivia("How much XP do you get \n for catching a Red Chinchompa?", "275", "232", "250", "265", "265"));
        questions.add(new Trivia("How much XP do you get \n for catching a Black Salamander?", "320", "180", "304", "286", "304"));
        questions.add(new Trivia("How much XP do you get \n for catching a Grey Chinchompa ?", "175", "198.25", "220", "190", "198.25"));
        questions.add(new Trivia("How much XP do you get \n for catching a Lobster?", "100", "80", "90", "70", "90"));
        questions.add(new Trivia("How much XP do you get \n for cooking a Lobster?", "100", "120", "110", "130", "120"));
        questions.add(new Trivia("How much XP do you get \nfor burning a Normal Log?", "25", "50", "35", "40", "40"));
        questions.add(new Trivia("How much XP do you get \nfor burning a Maple Log?", "135", "125", "150", "160", "135"));
        questions.add(new Trivia("How much XP do you get \nfor cooking a Karambwan Thouroughly?", "150", "175", "130", "190", "190"));
        questions.add(new Trivia("How much XP do you get \nfor casting High Alchemy", "60", "65", "67", "55", "65"));
        questions.add(new Trivia("How much XP do you get \nfor mining iron ore ?", "30", "25", "40", "35", "35"));
        questions.add(new Trivia("How much XP do you get \nfor mining runite ore?", "125", "150", "100", "112.5", "125"));
        questions.add(new Trivia("How much XP do you get \nfor mining clay?", "20", "10", "5", "15", "5"));
        questions.add(new Trivia("How much XP do you get \nfor cooking a Shark?", "200", "190", "250", "210", "210"));
        questions.add(new Trivia("How much XP do you get \nfor catching a shrimp?", "10", "15", "5", "20", "10"));
        questions.add(new Trivia("How much XP do you get \nfor cutting a Magic Tree?", "225", "200", "250", "275", "250"));
        questions.add(new Trivia("How much XP do you get \nfor making a Fire Battlestaff?", "135", "150", "130", "125", "125"));
        questions.add(new Trivia("How much XP do you get \nfor making a prayer potion?", "87.5", "82.5", "75", "90.5", "87.5"));
        questions.add(new Trivia("Which one of these cities \ndoesn't have a rooftop agility course?", "Ardougne", "Varrock", "Lumbridge", "Draynor", "Lumbridge"));
        questions.add(new Trivia("Which is not a boss \nin Desert Treasure?", "Damis", "Kamil", "Fareed", "Dessourt", "Dessourt"));
        questions.add(new Trivia("How many quest points does \nthe completion of RFD require?", "176", "182", "168", "200", "176"));
        questions.add(new Trivia("How much do Anchovies heal?", "3", "5", "2", "1", "1"));
        questions.add(new Trivia("How much do Lobsters heal?", "10", "13", "11", "12", "12"));
        questions.add(new Trivia("How much do Dark Crabs heal?", "20", "24", "22", "25", "22"));
        questions.add(new Trivia("How much do Tuna Potatoes heal?", "20", "22", "16", "21", "22"));
        questions.add(new Trivia("How much do Cooked Karambwan heal?", "15", "10", "20", "18", "18"));
        questions.add(new Trivia("How long is a game tick?", "1 second", ".5 seconds", ".6 seconds", ".4 seconds", ".6 seconds"));
        questions.add(new Trivia("How many game ticks between\n chances of Cutting a Log?", "5", "4", "3", "6", "4"));
        questions.add(new Trivia("How many game ticks between\n chances of catching a fish?", "4", "5", "3", "6", "5"));
        questions.add(new Trivia("How many Slayer Masters are in the game?", "5", "6", "7", "8", "6"));
        questions.add(new Trivia("What Slayer level are Skeletal Wyverns?", "70", "80", "75", "72", "72"));
        questions.add(new Trivia("What Slayer level are Abyssal Demons?", "85", "83", "80", "90", "85"));
        questions.add(new Trivia("What Slayer Level are Cave Kraken?", "85", "83", "87", "88", "87"));
        questions.add(new Trivia("What Slayer Level are Dust Devils?", "63", "65", "68", "70", "65"));
        questions.add(new Trivia("What Slayer Level are Jellys?", "52", "50", "60", "58", "52"));
        questions.add(new Trivia("What Slayer Level are Dark Beasts?", "92", "90", "88", "85", "90"));
        questions.add(new Trivia("What Slayer Level are Bloodveld?", "52", "40", "50", "45", "50"));
        questions.add(new Trivia("What Slayer Level are? Smoke Devil", "90", "95", "93", "87", "93"));
        questions.add(new Trivia("What Slayer Level \nare Aberrant spectres?", "65", "60", "62", "55", "60"));
        questions.add(new Trivia("What Slayer Level are Nechryael?", "75", "78", "80", "70", "80"));
        questions.add(new Trivia("Where is Duradel located?", "Lumbridge", "Meiyerditch", "Brimhaven", "Shilo Village", "Shilo Village"));
        questions.add(new Trivia("When was Old School Released?", "April 2013", "February 2013", "March 2013", "January 2014", "February 2013"));
        questions.add(new Trivia("What does Mod Ash do(besides be amazing)?", "Content Developer", "Quality Assurance", "Community Manager", "Artist", "Content Developer"));
        questions.add(new Trivia("What level do you need\n to teleport to Varrock?", "27", "28", "25", "31", "25"));
        questions.add(new Trivia("What level do you need\n to teleport to Annakarl?", "88", "90", "84", "78", "90"));
        questions.add(new Trivia("What level do you need\n to teleport to Falador?", "37", "33", "31", "41", "37"));
        questions.add(new Trivia("What level do you need\n to teleport to Camelot?", "41", "48", "40", "45", "45"));
        questions.add(new Trivia("What level do you need\n to teleport to Ardougne?", "51", "52", "48", "40", "51"));
        questions.add(new Trivia("What Level is Tz-Tok-Jad?", "720", "699", "360", "702", "702"));
        questions.add(new Trivia("How many Waves are in\n the fight caves?", "60", "50", "100", "63", "63"));
        questions.add(new Trivia("How much Strength \nbonus does a firecape give?", "1", "2", "4", "6", "4"));
        questions.add(new Trivia("What level defence do\n you need to wear Bandos?", "60", "65", "70", "75", "65"));
        questions.add(new Trivia("What level thieving is\n required for Desert Treasure?", "50", "63", "53", "48", "53"));
        questions.add(new Trivia("What is the highest level\n rooftop agility course?", "Relekka", "Ardougne", "Pollniveach", "Falador", "Ardougne"));
        questions.add(new Trivia("What level agility is \nrequired for the Ardougne course?", "90", "80", "85", "60", "90"));
        questions.add(new Trivia("What level agility is \nrequired for the Relekka course?", "90", "95", "80", "70", "80"));
        questions.add(new Trivia("What level agility is \nrequired for the Falador course?", "45", "40", "50", "60", "40"));
        questions.add(new Trivia("What level agility is \nrequired for the Varrock course?", "20", "10", "30", "40", "30"));
        questions.add(new Trivia("What level agility is \nrequired for the Pollniveach course?", "70", "80", "90", "60", "70"));
        questions.add(new Trivia("What level agility is \nrequired for the Ape Atoll course?", "70", "75", "48", "56", "48"));
        questions.add(new Trivia("What level agility is \nrequired for the Wilderness course?", "52", "54", "60", "45", "52"));
        questions.add(new Trivia("What level agility is \nrequired for the Dorgesh-Kaan course?", "70", "75", "85", "80", "70"));
        questions.add(new Trivia("What level Prayer do \nyou need to use Piety?", "60", "65", "70", "80", "70"));
        questions.add(new Trivia("What level Prayer do \nyou need to use Smite?", "60", "52", "50", "48", "52"));
        questions.add(new Trivia("What level Prayer do \nyou need to use Chivalry?", "65", "70", "60", "54", "60"));
        questions.add(new Trivia("What level Prayer do \nyou need to use Redemption?", "46", "49", "52", "35", "49"));
        questions.add(new Trivia("What level Prayer do \nyou need to use Protect from Missiles?", "40", "37", "43", "52", "40"));
        questions.add(new Trivia("What level Prayer do \nyou need to use Rapid Heal?", "22", "21", "19", "45", "22"));
        questions.add(new Trivia("How many Quest Points do \nyou get from completing Romeo and Juliet?", "1", "5", "3", "2", "5"));
        questions.add(new Trivia("What level thieving do you \nneed to open the chests at Rogue's Castle?", "80", "84", "88", "74", "84"));
        questions.add(new Trivia("What farming level to you need \nto plant Watermelons?", "47", "52", "40", "42", "47"));
        questions.add(new Trivia("What farming level to you \nneed to plant a Yew Tree?", "58", "60", "70", "50", "60"));
        questions.add(new Trivia("What farming level to you \nneed to plant a Calquat Tree?", "72", "75", "60", "58", "72"));
        questions.add(new Trivia("What farming level to you \nneed to plant Strawberries?", "35", "31", "25", "49", "31"));
        questions.add(new Trivia("What farming level to you \nneed to plant a Spirit Tree?", "60", "68", "83", "88", "83"));
        questions.add(new Trivia("What farming level to you \nneed to plant Torstol?", "65", "80", "90", "85", "85"));
        questions.add(new Trivia("What farming level to you \nneed to plant Ranarr?", "32", "38", "25", "28", "32"));
        questions.add(new Trivia("What herblore level do you \nneed to make a Supercombat Potion?", "80", "83", "60", "90", "90"));
        questions.add(new Trivia("What herblore level do you \nneed to make a Prayer Potion?", "28", "38", "25", "42", "38"));
        questions.add(new Trivia("What herblore level do you \nneed to make a Strength Potion?", "12", "1", "5", "8", "12"));
        questions.add(new Trivia("What herblore level do you \nneed to make a Super Restore Potion?", "70", "68", "63", "60", "63"));
        questions.add(new Trivia("What herblore level do you \nneed to make a Super Attack Potion?", "40", "45", "50", "31", "45"));
        questions.add(new Trivia("What herblore level do you \nneed to make a Saradomin Brew ?", "80", "81", "75", "84", "81"));
        questions.add(new Trivia("What is the Max Total Level?", "2277", "2260", "2188", "2319", "2277"));
        questions.add(new Trivia("Who was the first player to \nMax Total Level on Old Schoool?", "Sick Nerd", "Vestfold", "Jebrim", "User", "Jebrim"));
        questions.add(new Trivia("Who was the first player to \nreach 1Billion XP on Old School?", "Jebrim", "Zezima", "Vestfold", "Sick Nerd", "Vestfold"));
        questions.add(new Trivia("Who was the second player to \nreach 1Billion XP on Old School?", "Sick Nerd", "Jebrim", "Vestfold", "Hey Jase", "Sick Nerd"));
        questions.add(new Trivia("What skill was the first \n99 achieved in on Old School?", "Cooking", "Strength", "Fletching", "Thieving", "Thieving"));
        questions.add(new Trivia("What Runecrafting level \nunlocks double nature runes?", "93", "88", "91", "44", "91"));
        questions.add(new Trivia("What Level are the Melee Barrows Brothers?", "126", "110", "100", "115", "115"));
        questions.add(new Trivia("What combat level is \nthe King Black Dragon?", "276", "346", "333", "256", "276"));
        questions.add(new Trivia("What combat level is Corporeal Beast?", "800", "999", "785", "702", "785"));
        questions.add(new Trivia("Which spirit shield \nis not in Old School?", "Arcane", "Divine", "Elysian", "Spectral", "Divine"));
        questions.add(new Trivia("Which of these monsters \n don't drop a Dragon Chainbody?", "Kalphite Queen", "Dust Devil", "Chaos Elemental", "Smoke Devil", "Chaos Elemental"));
        questions.add(new Trivia("What Normal spells do \n you unlock at 80 magic?", "Charge and Stun", "Enfeeble and Stun", "Teleother Falador and Stun", "Charge and Teleblock", "Charge and Stun"));
        questions.add(new Trivia("Which skill did not work upon release on OSRS?", "Hunter", "Farming", "Construction", "Runecrafting", "Construction"));
        questions.add(new Trivia("How many spellbooks are in the game?", "1", "2", "3", "4", "3"));
        questions.add(new Trivia("Where is the apothecary?", "Varrock", "Lumbridge", "Seers", "Al Kharid", "Varrock"));
        questions.add(new Trivia("How many friends can you add?", "100", "200", "300", "400", "400"));
        questions.add(new Trivia("Which quest unlocks Crystal Equipment?", "Regicide", "Recipe for Disaster", "Legend's Quest", "Roving Elves", "Roving Elves"));
        questions.add(new Trivia("How many herb boxes can be bought daily from NMZ?", "15", "20", "50", "Unlimited", "15"));
        questions.add(new Trivia("Which of these Gods is not represented in the God Wars Dungeon?", "Zamorak", "Saradomin", "Guthix", "Bandos", "Guthix"));
        questions.add(new Trivia("Which of these cannot be a respawn point?", "Edgeville", "Camelot", "Falador", "Varrock", "Varrock"));
        questions.add(new Trivia("What drops the Seers Ring?", "Dagannoth Rex", "Dagannoth Prime", "Dagannoth King", "Dagannoth Supreme", "Dagannoth Prime"));
        questions.add(new Trivia("What is the Bounty Hunter world?", "W301", "W302", "W318", "W350", "W318"));
        questions.add(new Trivia("What kind of Dragon is Elvarg?", "Green", "Red", "Blue", "Black", "Green"));
        questions.add(new Trivia("What level is Elvarg?", "100", "200", "76", "86", "86"));
        questions.add(new Trivia("What Attack Style does Dagannoth Rex use?", "Magic", "Melee", "Range", "Mage/Range", "Melee"));
        questions.add(new Trivia("What Slayer Master can you find in Shilo Village?", "Nieve", "Vannaka", "Duradel", "Kuradel", "Duradel"));
        questions.add(new Trivia("Who is the highest level Slayer master?", "Duradel", "Nieve", "Vanaka", "Tureal", "Duradel"));


        a.setText(questions.get(1).option1);
        b.setText(questions.get(1).option2);
        c.setText(questions.get(1).option3);
        d.setText(questions.get(1).option4);

        question.setText(questions.get(1).getQuestion());

        currentQuestion = questions.get(1);


    }

    public void waveAnimation() {

        xpdrop.setVisibility(View.VISIBLE);

        PropertyValuesHolder tvThree_Y = PropertyValuesHolder.ofFloat(xpdrop.TRANSLATION_Y, -500.0f);
        PropertyValuesHolder tvThree_X = PropertyValuesHolder.ofFloat(xpdrop.TRANSLATION_X, 0);
        waveThreeAnimator = ObjectAnimator.ofPropertyValuesHolder(xpdrop, tvThree_X, tvThree_Y);
        waveThreeAnimator.setRepeatCount(0);
        waveThreeAnimator.setRepeatMode(ValueAnimator.REVERSE);
        waveThreeAnimator.setDuration(1000);

        //   waveThreeAnimator.setStartDelay(1000);
        waveThreeAnimator.start();


    }

    public void resetAnimation() {

        xpdrop.setVisibility(View.GONE);

        PropertyValuesHolder tvThree_Y = PropertyValuesHolder.ofFloat(xpdrop.TRANSLATION_Y, 200.0f);
        PropertyValuesHolder tvThree_X = PropertyValuesHolder.ofFloat(xpdrop.TRANSLATION_X, 0);
        waveThreeAnimator = ObjectAnimator.ofPropertyValuesHolder(xpdrop, tvThree_X, tvThree_Y);
        waveThreeAnimator.setRepeatCount(0);
        waveThreeAnimator.setRepeatMode(ValueAnimator.REVERSE);
        waveThreeAnimator.setDuration(1000);

        waveThreeAnimator.setStartDelay(0);
        waveThreeAnimator.start();


    }


    @Override
    public void onClick(View v) {


        switch (v.getId()) {
            case R.id.answer1:


                if(x == 0) {
                    if (a.getText().equals(currentQuestion.getAnswer())) {

                        a.setBackgroundResource(R.drawable.mybuttongreen);
                        waveAnimation();
                        LoginDialog newFragment = new LoginDialog();
                        newFragment.show(getFragmentManager(), "Login");
                        isStreak = true;
                        points = points+100;
                        if(isStreak) {
                            currentStreak++;
                        }
                        totalCorrect++;
                    } else {
                        a.setBackgroundResource(R.drawable.mybuttonred);
                        isStreak = false;
                        if (longestStreak < currentStreak)
                            longestStreak = currentStreak;
                        currentStreak = 0;
                    }


                    alert = new AlertDialog.Builder(this);


                    //alert.setIcon(R.drawable.correct);
// Set an EditText view to get user input


                    alert.setPositiveButton("Next Question", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            a.setBackgroundResource(R.drawable.bkg);
                            newQuestion();
                            dialog.cancel();

                            resetAnimation();
                            // Do something with value!
                        }
                    });


                    alert.show();
                }
                x++;
                break;

            case R.id.answer2:
                if(x == 0) {
                    if (b.getText().equals(currentQuestion.getAnswer())) {
                        b.setBackgroundResource(R.drawable.mybuttongreen);
                        waveAnimation();LoginDialog newFragment = new LoginDialog();
                        newFragment.show(getFragmentManager(), "Login");
                        isStreak = true;
                        points = points+100;
                        if(isStreak) {
                            currentStreak++;
                        }
                        totalCorrect++;
                    } else {
                        b.setBackgroundResource(R.drawable.mybuttonred);
                        isStreak = false;
                        if (longestStreak < currentStreak)
                            longestStreak = currentStreak;
                        currentStreak = 0;
                    }

                    alert = new AlertDialog.Builder(this);


                    alert.setPositiveButton("Next Question", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            b.setBackgroundResource(R.drawable.bkg);
                            dialog.cancel();
                            newQuestion();
                            resetAnimation();
                            // Do something with value!
                        }
                    });

                    alert.show();
                }
                x++;
                break;

            case R.id.answer3:
                if(x == 0) {
                    if (c.getText().equals(currentQuestion.getAnswer())) {
                        c.setBackgroundResource(R.drawable.mybuttongreen);
                        waveAnimation();
                        LoginDialog newFragment = new LoginDialog();
                        newFragment.show(getFragmentManager(), "Login");
                        isStreak = true;
                        points = points+100;
                        if(isStreak) {
                            currentStreak++;
                        }
                        totalCorrect++;
                    } else {
                        c.setBackgroundResource(R.drawable.mybuttonred);
                        isStreak = false;
                        if (longestStreak < currentStreak)
                            longestStreak = currentStreak;
                        currentStreak = 0;
                    }

                    alert = new AlertDialog.Builder(this);


                    alert.setPositiveButton("Next Question", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            c.setBackgroundResource(R.drawable.bkg);
                            xpdrop.setVisibility(View.GONE);
                            resetAnimation();

                            newQuestion();
                            dialog.cancel();

                            // Do something with value!
                        }
                    });


                    alert.show();

                }
                x++;
                break;

            case R.id.answer4:
                if(x == 0) {
                    if (d.getText().equals(currentQuestion.getAnswer())) {
                        d.setBackgroundResource(R.drawable.mybuttongreen);
                        waveAnimation();
                        LoginDialog newFragment = new LoginDialog();
                        newFragment.show(getFragmentManager(), "Login");
                        isStreak = true;
                        points = points+100;
                        if(isStreak) {
                            currentStreak++;
                        }
                        totalCorrect++;

                    } else {
                        d.setBackgroundResource(R.drawable.mybuttonred);
                        isStreak = false;
                        if (longestStreak < currentStreak)
                            longestStreak = currentStreak;
                        currentStreak = 0;
                    }
                    alert = new AlertDialog.Builder(this);


                    alert.setPositiveButton("Next Question", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            d.setBackgroundResource(R.drawable.bkg);
                            newQuestion();
                            dialog.cancel();
                            resetAnimation();
                            // Do something with value!
                        }
                    });

                    alert.show();
                }

                x++;
                break;


        }





    }
    public void newQuestion()

    {
        totalQ ++;
        if(questions.size()<1)
        {
            alert.setPositiveButton("Game Over", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    a.setBackgroundResource(R.drawable.bkg);
                    // newQuestion();
                    dialog.cancel();

                    resetAnimation();
                    // Do something with value!
                }
            });


            alert.show();
            a.setText("total questions: " + totalQ);
            b.setText("longest Streak: "+ longestStreak);
            c.setText("Total correct: "+ totalCorrect);
            d.setText("Total Points: "+ points);
        }
        else {
            Random pick = new Random();
            int x1 = pick.nextInt(questions.size());

            currentQuestion = questions.get(x1);
            String[] answers = currentQuestion.getOptions();


            question.setText(currentQuestion.getQuestion());
            a.setText(answers[0]);
            b.setText(answers[1]);
            c.setText(answers[2]);
            d.setText(answers[3]);
            x = 0;

            questions.remove(x1);

        }
    }

    public class LoginDialog extends DialogFragment {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            // Get the layout inflater
            LayoutInflater inflater = getActivity().getLayoutInflater();

            // Inflate and set the layout for the dialog
            // Pass null as the parent view because its going in the dialog layout
            final View view = inflater.inflate(R.layout.correctdialog, null);
            //    final EditText RSN = (EditText) view.findViewById(R.id.rsn);
            builder.setView(view)
                    // Add action buttons
                    .setPositiveButton("Next Question", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            a.setBackgroundResource(R.drawable.bkg);
                            b.setBackgroundResource(R.drawable.bkg);
                            c.setBackgroundResource(R.drawable.bkg);
                            d.setBackgroundResource(R.drawable.bkg);

                            newQuestion();
                            dialog.cancel();
                            resetAnimation();




                            /*String filename = "mysecondfile";
                            String outputString = RSN.getText().toString();
                            File myDir = getFilesDir();

                            try {
                                File secondFile = new File(myDir + "/text/", filename);
                                if (secondFile.getParentFile().mkdirs()) {
                                    secondFile.createNewFile();
                                    FileOutputStream fos = new FileOutputStream(secondFile);

                                    fos.write(outputString.getBytes());
                                    fos.flush();
                                    fos.close();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }*/


                            // sign in the user ...
                        }
                    });
            return builder.create();
        }

        public void confirmLogin() {
            LoginDialog newFragment = new LoginDialog();
            newFragment.show(getFragmentManager(), "Login");
        }

    }

}