package com.matt1.thenewboston;

import android.app.Activity;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.animation.ScaleAnimation;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ScrollView;

/**
 * Created by Matthew on 1/1/2015.
 */
public class Map extends Activity {

    private float mScale = 1f;
    public ScaleGestureDetector mScaleDetector;
    GestureDetector gestureDetector;
    WebView map;
    ScrollView layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map);





        map = (WebView) findViewById(R.id.mapview);

        map.loadUrl("file:///android_asset/newmap.html");
        map.getSettings().setBuiltInZoomControls(true);

        map.getSettings().setLoadWithOverviewMode(true);
        map.getSettings().setUseWideViewPort(true);






















//        layout =(ScrollView) findViewById(R.id.scrollView2);
////step 2: create instance from GestureDetector(this step sholude be place into onCreate())
//        gestureDetector = new GestureDetector(this, new GestureListener());
//
//// animation for scalling
//        mScaleDetector = new ScaleGestureDetector(this, new ScaleGestureDetector.SimpleOnScaleGestureListener()
//        {
//            @Override
//            public boolean onScale(ScaleGestureDetector detector)
//            {
//                float scale = 1 - detector.getScaleFactor();
//
//                float prevScale = mScale;
//                mScale += scale;
//
//                if (mScale < 0.1f) // Minimum scale condition:
//                    mScale = 0.1f;
//
//                if (mScale > 10f) // Maximum scale condition:
//                    mScale = 10f;
//                ScaleAnimation scaleAnimation = new ScaleAnimation(1f / prevScale, 1f / mScale, 1f / prevScale, 1f / mScale, detector.getFocusX(), detector.getFocusY());
//                scaleAnimation.setDuration(0);
//                scaleAnimation.setFillAfter(true);
//              //  ScrollView layout =(ScrollView) findViewById(R.id.scrollView2);
//                layout.startAnimation(scaleAnimation);
//                return true;
//            }
//        });
//
//
//
//    }
//
//
//
//
//
//    // step 3: override dispatchTouchEvent()
//    @Override
//    public boolean dispatchTouchEvent(MotionEvent event) {
//        super.dispatchTouchEvent(event);
//        mScaleDetector.onTouchEvent(event);
//        gestureDetector.onTouchEvent(event);
//        return gestureDetector.onTouchEvent(event);
//    }
//
////step 4: add private class GestureListener
//
//    private class GestureListener extends GestureDetector.SimpleOnGestureListener {
//        @Override
//        public boolean onDown(MotionEvent e) {
//            return true;
//        }
//        // event when double tap occurs
//        @Override
//        public boolean onDoubleTap(MotionEvent e) {
//            // double tap fired.
//            return true;
//        }
//    }


    }

}