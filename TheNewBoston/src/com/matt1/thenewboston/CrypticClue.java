package com.matt1.thenewboston;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;



public class CrypticClue extends Activity implements View.OnClickListener {

    ArrayList<String> clue = new ArrayList<String>();
    ArrayList<String> answer = new ArrayList<String>();
    private static final String TAG = PriceGuide.class.getSimpleName();

    String url = "http://2007.runescape.wikia.com/wiki/Treasure_Trails/Guide/Cryptic_clues";
    ListView clueView;
    Spinner letters;

    EditText clueQuery;
    ImageButton searchClue;
    String starts;

    TextView cryp;
    BaseAdapter2 adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.crypticclue);

        clueQuery = (EditText) findViewById(R.id.clueQuery);

        clueQuery.setInputType(android.text.InputType.TYPE_CLASS_TEXT | android.text.InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        searchClue = (ImageButton) findViewById(R.id.searchClue);

        cryp = (TextView) findViewById(R.id.crypticcluetitle);
        Typeface myTypeface = Typeface.createFromAsset(getAssets(), "fonts/runescape_uf.ttf");


        cryp.setTypeface(myTypeface);





        clueView = (ListView) findViewById(R.id.crypticList);


        searchClue.setOnClickListener(this);




    }


    @Override
    public void onClick(View view) {
        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);


        starts = clueQuery.getText().toString();

        clueView.setAdapter(null);


        new TheTask().execute();

    }


    class TheTask extends AsyncTask<Void, Void, String> {
        Elements content, clues;
        @Override
        protected String doInBackground(Void... params) {
            try {
                // Connect to the web site
                Document document = Jsoup.connect(url).get();
                // Get the html document title

                content = document.select("table");
                clues = content.select("tr");
                clues = clues.select("td");
                //title = document.title();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;


        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);



            clue.clear();

            answer.clear();
            for(int i = 0; i<clues.size(); i++) {





                if (clues.get(i).text().startsWith(starts)) {
                    if(clues.get(i+1).text().length()>0) {
                        clue.add(clues.get(i).text());

                        answer.add(clues.get(i + 1).text());
                    }
                }
            }


            adapter = new BaseAdapter2(CrypticClue.this, clue, answer);

            clueView.setAdapter(adapter);

        }
    }


    public class BaseAdapter2 extends BaseAdapter {

        private Activity activity;
        // private ArrayList&lt;HashMap&lt;String, String&gt;&gt; data;
        private ArrayList clues, answers;
        private ArrayList<Drawable> images;

        private final String TAG = BaseAdapter2.class.getSimpleName();
        private LayoutInflater inflater = null;
        String imageURL;
        Bitmap mIcon_val;
        Drawable please;

        int x = 0;
        ArrayList<Drawable> image = new ArrayList<Drawable>();


        public BaseAdapter2(Activity a, ArrayList c, ArrayList d) {
            activity = a;
            this.clues = c;
            this.answers = d;


            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }


        public int getCount() {
            return clues.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, final ViewGroup parent) {
            View vi = convertView;
            if (convertView == null)
                vi = inflater.inflate(R.layout.cryptic_clue_item, null);



            TextView title2 = (TextView) vi.findViewById(R.id.crypticclue); // title
            final String item = clues.get(position).toString();
            title2.setText(item);


            TextView title22 = (TextView) vi.findViewById(R.id.crypticanswer); // notice
            String average = answers.get(position).toString();
            title22.setText(average);



            return vi;

        }


    }




}