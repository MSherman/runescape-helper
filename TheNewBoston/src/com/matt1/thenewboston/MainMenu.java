package com.matt1.thenewboston;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Random;

/**
 * Created by Matthew on 12/20/2014.
 */
public class MainMenu extends Activity implements View.OnClickListener {

    TextView question, menutitle;
    Trivia[] questions = new Trivia[501];
    Button hs, triv, pg, cc, alerts, quest, twit, worldmap, calcs, boss, resources;

    String rsname = "";
    ImageView correct;

    Trivia currentQuestion;
    AlertDialog.Builder alert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainmenu);

        LoginDialog2 newFragment = new LoginDialog2();

        String filename = "mysecondfile";

        File myDir = getFilesDir();



        try {
            File secondInputFile = new File(myDir + "/text/", filename);
            InputStream secondInputStream = new BufferedInputStream(new FileInputStream(secondInputFile));
            BufferedReader r = new BufferedReader(new InputStreamReader(secondInputStream));
            StringBuilder total = new StringBuilder();
            String line;
            while ((line = r.readLine()) != null) {
                total.append(line);
            }
            r.close();
            secondInputStream.close();
            rsname = total.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }



        if(rsname.length()<1)
            newFragment.show(getFragmentManager(), "Login");


        menutitle = (TextView) findViewById(R.id.menutitle);

        hs = (Button) findViewById(R.id.mmHiScores);
        triv = (Button) findViewById(R.id.mmTrivia);
        pg = (Button) findViewById(R.id.mmPriceGuide);
        cc = (Button) findViewById(R.id.mmClues);
        alerts = (Button) findViewById(R.id.mmAlerts);
        quest = (Button) findViewById(R.id.mmQuestguide);
        twit = (Button) findViewById(R.id.mmTwitter);
        worldmap = (Button) findViewById(R.id.mmWorldmap);
        calcs = (Button) findViewById(R.id.mmCalcs);
        boss = (Button) findViewById(R.id.mmDropLogger);
        resources = (Button) findViewById(R.id.mmResources);

        hs.setOnClickListener(this);
        triv.setOnClickListener(this);
        pg.setOnClickListener(this);
        cc.setOnClickListener(this);
        alerts.setOnClickListener(this);
        quest.setOnClickListener(this);
        twit.setOnClickListener(this);
        worldmap.setOnClickListener(this);
        calcs.setOnClickListener(this);
        boss.setOnClickListener(this);
        resources.setOnClickListener(this);


        Typeface myTypeface = Typeface.createFromAsset(getAssets(), "fonts/runescape_uf.ttf");


        hs.setTypeface(myTypeface);
        triv.setTypeface(myTypeface);
        pg.setTypeface(myTypeface);
        cc.setTypeface(myTypeface);
        alerts.setTypeface(myTypeface);
        quest.setTypeface(myTypeface);
        twit.setTypeface(myTypeface);
        worldmap.setTypeface(myTypeface);
        calcs.setTypeface(myTypeface);
        boss.setTypeface(myTypeface);
        resources.setTypeface(myTypeface);
        menutitle.setTypeface(myTypeface);

    }
    @Override
    public void onClick(View v) {

        Class ourClass;
        Intent ourIntent;

        switch (v.getId()) {
            case R.id.mmHiScores:


                try {
                    ourClass = Class.forName("com.matt1.thenewboston.RevampedScores");
                    ourIntent = new Intent(MainMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;

            case R.id.mmTrivia:


                try {
                    ourClass = Class.forName("com.matt1.thenewboston.TriviaGame");
                    ourIntent = new Intent(MainMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;

            case R.id.mmPriceGuide:


                try {
                    ourClass = Class.forName("com.matt1.thenewboston.PriceGuide");
                    ourIntent = new Intent(MainMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;

            case R.id.mmClues:

                try {
                    ourClass = Class.forName("com.matt1.thenewboston.ClueMenu");
                    ourIntent = new Intent(MainMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;


            case R.id.mmAlerts:


                try {
                    ourClass = Class.forName("com.matt1.thenewboston.Alerts");
                    ourIntent = new Intent(MainMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;


            case R.id.mmQuestguide:

                try {
                    ourClass = Class.forName("com.matt1.thenewboston.QuestGuides");
                    ourIntent = new Intent(MainMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;

            case R.id.mmTwitter:

                try {
                    ourClass = Class.forName("com.matt1.thenewboston.Tweets");
                    ourIntent = new Intent(MainMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;

            case R.id.mmWorldmap:


                try {
                    ourClass = Class.forName("com.matt1.thenewboston.Map");
                    ourIntent = new Intent(MainMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;

            case R.id.mmCalcs:


                try {
                    ourClass = Class.forName("com.matt1.thenewboston.CalcMenu");
                    ourIntent = new Intent(MainMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;

            case R.id.mmDropLogger:


                try {
                    ourClass = Class.forName("com.matt1.thenewboston.BossMenu");
                    ourIntent = new Intent(MainMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;

            case R.id.mmResources:


                try {
                    ourClass = Class.forName("com.matt1.thenewboston.Resources");
                    ourIntent = new Intent(MainMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;


        }
    }

        public class LoginDialog2 extends DialogFragment {

            @Override
            public Dialog onCreateDialog(Bundle savedInstanceState) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                // Get the layout inflater
                LayoutInflater inflater = getActivity().getLayoutInflater();

                // Inflate and set the layout for the dialog
                // Pass null as the parent view because its going in the dialog layout
                final View view = inflater.inflate(R.layout.dialog_signin, null);
                final EditText RSN = (EditText) view.findViewById(R.id.rsn);
                RSN.setTextColor(Color.WHITE);
                builder.setView(view)
                        // Add action buttons
                        .setPositiveButton("Save RSN", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {

                                String filename = "mysecondfile";
                                String outputString = RSN.getText().toString();
                                File myDir = getFilesDir();

                                try {
                                    File secondFile = new File(myDir + "/text/", filename);
                                    if (secondFile.getParentFile().mkdirs()) {
                                        secondFile.createNewFile();
                                        FileOutputStream fos = new FileOutputStream(secondFile);

                                        fos.write(outputString.getBytes());

                                        fos.flush();
                                        fos.close();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }


                                // sign in the user ...
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                LoginDialog2.this.getDialog().cancel();
                            }
                        });
                return builder.create();
            }

            public void confirmLogin() {
                LoginDialog2 newFragment = new LoginDialog2();
                newFragment.show(getFragmentManager(), "Login");
            }
        }





    }





