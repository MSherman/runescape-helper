package com.matt1.thenewboston;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class BossMenu extends Activity implements View.OnClickListener {

    TextView  menutitle;

    ImageButton rex, supreme, prime, callisto, venenatis, vetion, zilyana, kreearra, graardor, kril, kraken, smokedevil, scorpia, kbd, kq, chaosele, giantmole, zulrah;
    Class ourClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choose_boss);

        menutitle = (TextView) findViewById(R.id.menutitle2);

        rex = (ImageButton) findViewById(R.id.dropRex);
        supreme = (ImageButton) findViewById(R.id.dropSupreme);
        prime = (ImageButton) findViewById(R.id.dropPrime);
        callisto = (ImageButton) findViewById(R.id.dropCallisto);
        venenatis = (ImageButton) findViewById(R.id.dropVenenatis);
        vetion = (ImageButton) findViewById(R.id.dropVetion);
        zilyana = (ImageButton) findViewById(R.id.dropCommanderZilyana);
        kreearra = (ImageButton) findViewById(R.id.dropKreearra);
        graardor = (ImageButton) findViewById(R.id.dropGraardor);
        kril = (ImageButton) findViewById(R.id.dropKril);
        kraken = (ImageButton) findViewById(R.id.dropKraken);
        smokedevil = (ImageButton) findViewById(R.id.dropThermo);
        scorpia = (ImageButton) findViewById(R.id.dropScorpia);
        kbd = (ImageButton) findViewById(R.id.dropKingBlackDragon);
        kq = (ImageButton) findViewById(R.id.dropKalphiteQueen);
        chaosele = (ImageButton) findViewById(R.id.dropChaosEle);
        giantmole = (ImageButton) findViewById(R.id.dropGiantMole);
        zulrah = (ImageButton) findViewById(R.id.dropZulrah);



        rex.setOnClickListener(this);
        supreme.setOnClickListener(this);
        prime.setOnClickListener(this);
        callisto.setOnClickListener(this);
        venenatis.setOnClickListener(this);
        vetion.setOnClickListener(this);
        zilyana.setOnClickListener(this);
        kreearra.setOnClickListener(this);
        graardor.setOnClickListener(this);
        kril.setOnClickListener(this);
        kraken.setOnClickListener(this);
        smokedevil.setOnClickListener(this);
        scorpia.setOnClickListener(this);
        kbd.setOnClickListener(this);
        kq.setOnClickListener(this);
        chaosele.setOnClickListener(this);
        giantmole.setOnClickListener(this);
        zulrah.setOnClickListener(this);

        Typeface myTypeface = Typeface.createFromAsset(getAssets(), "fonts/runescape_uf.ttf");

        menutitle.setTypeface(myTypeface);



    }
    @Override
    public void onClick(View v) {

        switch (v.getId()) {




            case R.id.dropRex:


                try {
                    ourClass = Class.forName("com.matt1.thenewboston.DropLog");
                    Intent ourIntent = new Intent(BossMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;

            case R.id.dropSupreme:


                try {
                    ourClass = Class.forName("com.matt1.thenewboston.DagannothSupreme");
                    Intent ourIntent = new Intent(BossMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;

            case R.id.dropPrime:


                try {
                    ourClass = Class.forName("com.matt1.thenewboston.DagannothPrime");
                    Intent ourIntent = new Intent(BossMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;

            case R.id.dropCallisto:


                try {
                    ourClass = Class.forName("com.matt1.thenewboston.Callisto");
                    Intent ourIntent = new Intent(BossMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;

            case R.id.dropVenenatis:


                try {
                    ourClass = Class.forName("com.matt1.thenewboston.Venenatis");
                    Intent ourIntent = new Intent(BossMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;

            case R.id.dropVetion:


                try {
                    ourClass = Class.forName("com.matt1.thenewboston.Vetion");
                    Intent ourIntent = new Intent(BossMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;

            case R.id.dropCommanderZilyana:


                try {
                    ourClass = Class.forName("com.matt1.thenewboston.Zilyana");
                    Intent ourIntent = new Intent(BossMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;

            case R.id.dropKreearra:


                try {
                    ourClass = Class.forName("com.matt1.thenewboston.Kreearra");
                    Intent ourIntent = new Intent(BossMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;

            case R.id.dropGraardor:


                try {
                    ourClass = Class.forName("com.matt1.thenewboston.Graardor");
                    Intent ourIntent = new Intent(BossMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;

            case R.id.dropKril:


                try {
                    ourClass = Class.forName("com.matt1.thenewboston.Kril");
                    Intent ourIntent = new Intent(BossMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;

            case R.id.dropKraken:


                try {
                    ourClass = Class.forName("com.matt1.thenewboston.Kraken");
                    Intent ourIntent = new Intent(BossMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;

            case R.id.dropThermo:


                try {
                    ourClass = Class.forName("com.matt1.thenewboston.Thermo");
                    Intent ourIntent = new Intent(BossMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;

            case R.id.dropScorpia:


                try {
                    ourClass = Class.forName("com.matt1.thenewboston.Scorpia");
                    Intent ourIntent = new Intent(BossMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;

            case R.id.dropKingBlackDragon:


                try {
                    ourClass = Class.forName("com.matt1.thenewboston.KingBlackDragon");
                    Intent ourIntent = new Intent(BossMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;

            case R.id.dropKalphiteQueen:


                try {
                    ourClass = Class.forName("com.matt1.thenewboston.KalphiteQueen");
                    Intent ourIntent = new Intent(BossMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;

            case R.id.dropChaosEle:


                try {
                    ourClass = Class.forName("com.matt1.thenewboston.ChaosEle");
                    Intent ourIntent = new Intent(BossMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;

            case R.id.dropGiantMole:


                try {
                    ourClass = Class.forName("com.matt1.thenewboston.GiantMole");
                    Intent ourIntent = new Intent(BossMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;


            case R.id.dropZulrah:


                try {
                    ourClass = Class.forName("com.matt1.thenewboston.Zulrah");
                    Intent ourIntent = new Intent(BossMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;
        }

        }





    }





