package com.matt1.thenewboston;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

public class PriceGuide extends Activity implements View.OnClickListener, AdapterView.OnItemClickListener{

    ArrayList<String> image_array = new ArrayList<String>();
    ArrayList<String> item_array = new ArrayList<String>();
    ArrayList<String> average_array = new ArrayList<String>();
    ArrayList<String> alch_array = new ArrayList<String>();
    private static final String TAG = PriceGuide.class.getSimpleName();

    ArrayList<Drawable> images = new ArrayList<Drawable>();

    String itemQuery;

    ImageButton lookup;

    TextView title;
    EditText search;
    int x;
    int y;
    ListView list;

    BaseAdapter2 adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.priceguide);

        search = (EditText) findViewById(R.id.priceItem);

        title = (TextView) findViewById(R.id.pricetitle);
        Typeface myTypeface = Typeface.createFromAsset(getAssets(), "fonts/runescape_uf.ttf");

        title.setTypeface(myTypeface);


        search.setInputType(android.text.InputType.TYPE_CLASS_TEXT | android.text.InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

        lookup = (ImageButton) findViewById(R.id.priceSearch);
        lookup.setOnClickListener(this);


        list = (ListView) findViewById(R.id.listView1);


            list.setOnItemClickListener(this);
    }

    @Override
    public void onClick(View view) {
        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);

        itemQuery = search.getText().toString().replace(" ", "_");

        list.setAdapter(null);
        images.clear();
        x = 0;
        y = 0;
        new TheTask().execute();

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

    }


    class TheTask extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {
            String str = null;
            try {
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(
                        "http://forums.zybez.net/runescape-2007-prices/api/" + itemQuery);
                HttpResponse response = httpclient.execute(httppost);
                str = EntityUtils.toString(response.getEntity());
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return str;

        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            String response = result.toString();
            try {


                JSONArray new_array = new JSONArray(response);

                image_array.clear();
                item_array.clear();
                average_array.clear();
                alch_array.clear();






                for (int i = 0, count = new_array.length(); i < count; i++) {
                    try {
                        JSONObject jsonObject = new_array.getJSONObject(i);
                        image_array.add(jsonObject.getString("image").toString());
                        item_array.add(jsonObject.getString("name").toString());
                        average_array.add(jsonObject.getString("average").toString());
                        alch_array.add(jsonObject.getString("high_alch").toString());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


                new AddImages().execute();

                while (x < image_array.size())
                    y++;

                adapter = new BaseAdapter2(PriceGuide.this, images, item_array, average_array, alch_array);

                list.setAdapter(adapter);



            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                // tv.setText("error2");
            }

        }
    }

    class AddImages extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            String str = null;


            for (int i = 0; i < image_array.size(); i++) {
                images.add(LoadImage(image_array.get(i).toString().replace("\\/", "")));
                x++;
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void result) {


        }

    }

    public static Drawable LoadImage(String url) {
        try {
            InputStream is = (InputStream) new URL(url).getContent();
            Drawable d = Drawable.createFromStream(is, "src name");
            return d;
        } catch (Exception e) {
            return null;
        }
    }

    public class BaseAdapter2 extends BaseAdapter {

        private Activity activity;
        // private ArrayList&lt;HashMap&lt;String, String&gt;&gt; data;
        private ArrayList itemName, price, highalch;
        private ArrayList<Drawable> images;


        private final String TAG = BaseAdapter2.class.getSimpleName();
        private LayoutInflater inflater = null;
        String imageURL;
        Bitmap mIcon_val;
        String exactitem;
        Drawable please;

        ArrayList<String> sell_array = new ArrayList<String>();
        ArrayList<String> quantity_array = new ArrayList<String>();
        ArrayList<String> price_array = new ArrayList<String>();
        ArrayList<String> rsn_array = new ArrayList<String>();
        int x = 0;
        ArrayList<Drawable> image = new ArrayList<Drawable>();


        public BaseAdapter2(Activity a, ArrayList<Drawable> b, ArrayList bod, ArrayList c, ArrayList d) {
            activity = a;
            this.images = b;
            this.itemName = bod;
            this.price = c;
            this.highalch = d;


            //image.add(LoadImage(images.get(i).toString().replace("\\/", "")));


            x = 0;

            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }


        public int getCount() {
            return itemName.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, final ViewGroup parent) {
            View vi = convertView;
            if (convertView == null)
                vi = inflater.inflate(R.layout.rowlist_item, null);

            ImageButton itemImage = (ImageButton) vi.findViewById(R.id.iv_icon_social);
            please = images.get(position);

            x = position;


            itemImage.setImageDrawable(please);
            itemImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    exactitem = itemName.get(position).toString().replace(" ", "+");
                    new TheTask2().execute();


                }
            });


            TextView title2 = (TextView) vi.findViewById(R.id.txt_ttlsm_row); // title
            final String item = itemName.get(position).toString();
            title2.setText(item);


            TextView title22 = (TextView) vi.findViewById(R.id.txt_ttlcontact_row2); // notice
            String average = price.get(position).toString();
            title22.setText("Average Price:" + average);

            final TextView title222 = (TextView) vi.findViewById(R.id.txt_ttlcontact_row3); // notice
            String alch = highalch.get(position).toString();
            title222.setText("High Alch Value:" + alch);


            return vi;

        }


        class TheTask2 extends AsyncTask<Void, Void, String> {

            @Override
            protected String doInBackground(Void... params) {
                String str = null;
                try {
                    HttpClient httpclient = new DefaultHttpClient();
                    HttpPost httppost = new HttpPost(
                            "http://forums.zybez.net/runescape-2007-prices/api/" + exactitem);
                    HttpResponse response = httpclient.execute(httppost);
                    str = EntityUtils.toString(response.getEntity());
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                return str;

            }


            @Override
            protected void onPostExecute(String result) {

                super.onPostExecute(result);

                String response = result.toString();
                try {


                    JSONArray new_array2 = new JSONArray(response);

                    sell_array.clear();
                    quantity_array.clear();
                    price_array.clear();
                    rsn_array.clear();

                        JSONObject js = new_array2.getJSONObject(0);
                        JSONArray bah = js.getJSONArray("offers");

                   //     JSONObject js2 = bah.getJSONObject(0);

                //        tester.setText(bah.toString().);



            //        JSONArray fuckthis = new JSONArray(hacs);


              //      tester.setText(bah.toString());


                    for (int i = 0, count = bah.length(); i < count; i++) {
                        try {
                            JSONObject jsonObject = bah.getJSONObject(i);
                            price_array.add(jsonObject.getString("price").toString());
                            sell_array.add(jsonObject.getString("selling").toString());
                            rsn_array.add(jsonObject.getString("rs_name").toString());
                            quantity_array.add(jsonObject.getString("quantity").toString());


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }



                    BaseAdapter5 adapter5 = new BaseAdapter5(PriceGuide.this, sell_array, quantity_array, price_array, rsn_array);

                    list.setAdapter(null);

                    list.setAdapter(adapter5);


                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    // tv.setText("error2");
                }

            }
        }
            public class BaseAdapter5 extends BaseAdapter {

                private Activity activity;
                // private ArrayList&lt;HashMap&lt;String, String&gt;&gt; data;
                private ArrayList<String> sellbuy, quantity, prices, rs_name;


                private final String TAG = BaseAdapter2.class.getSimpleName();
                private LayoutInflater inflater = null;
                String imageURL;
                Bitmap mIcon_val;
                String exactitem;
                Drawable please;

                ArrayList<String> sell_array = new ArrayList<String>();
                ArrayList<String> quantity_array = new ArrayList<String>();
                ArrayList<String> price_array = new ArrayList<String>();
                ArrayList<String> rsn_array = new ArrayList<String>();
                int x = 0;
                ArrayList<Drawable> image = new ArrayList<Drawable>();


                public BaseAdapter5(Activity a, ArrayList b, ArrayList bod, ArrayList c, ArrayList d) {
                    activity = a;
                    this.sellbuy = b;
                    this.quantity = bod;
                    this.prices = c;
                    this.rs_name = d;


                    //image.add(LoadImage(images.get(i).toString().replace("\\/", "")));


                    x = 0;

                    inflater = (LayoutInflater) activity
                            .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                }


                public int getCount() {
                    return sellbuy.size();
                }

                public Object getItem(int position) {
                    return position;
                }

                public long getItemId(int position) {
                    return position;
                }

                public View getView(final int position, View convertView, final ViewGroup parent) {
                    View vi = convertView;
                    if (convertView == null)
                        vi = inflater.inflate(R.layout.specificitem, null);

                    TextView itemImage = (TextView) vi.findViewById(R.id.sellorbuy);
                    String s = sellbuy.get(position).toString();

                    if(s.equals("1")) {
                        itemImage.setTextColor(Color.GREEN);
                        itemImage.setText("Selling ");
                    }else {
                        itemImage.setTextColor(Color.RED);
                        itemImage.setText("Buying");
                    }
                    TextView title2 = (TextView) vi.findViewById(R.id.quantity); // title
                    final String item = quantity.get(position).toString();
                    title2.setText(item);


                    TextView title22 = (TextView) vi.findViewById(R.id.price); // notice
                    String average = prices.get(position).toString();
                    title22.setText("@ " + average);

                    final TextView title222 = (TextView) vi.findViewById(R.id.rs_name); // notice
                    String alch = rs_name.get(position).toString();
                    title222.setText("    " + alch);


                    return vi;

                }


            }


        }



}