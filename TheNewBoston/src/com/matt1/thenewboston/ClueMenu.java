package com.matt1.thenewboston;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;

/**
 * Created by Matthew on 12/20/2014.
 */
public class ClueMenu extends Activity implements View.OnClickListener {


    Button cc, ana, maps;


    TextView title;


    AlertDialog.Builder alert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cluemenu);


        cc = (Button) findViewById(R.id.cmCryticClues);
        ana = (Button) findViewById(R.id.cmAnagrams);
        maps = (Button) findViewById(R.id.cmMapclues);



        cc.setOnClickListener(this);
        ana.setOnClickListener(this);
        maps.setOnClickListener(this);


        title = (TextView) findViewById(R.id.cluemenutitle);

        Typeface myTypeface = Typeface.createFromAsset(getAssets(), "fonts/runescape_uf.ttf");


        cc.setTypeface(myTypeface);
        ana.setTypeface(myTypeface);
        maps.setTypeface(myTypeface);
        title.setTypeface(myTypeface);



    }
    @Override
    public void onClick(View v) {


        switch (v.getId()) {
            case R.id.cmCryticClues:

                Class ourClass;
                try {
                    ourClass = Class.forName("com.matt1.thenewboston.CrypticClue");
                    Intent ourIntent = new Intent(ClueMenu.this, ourClass);
                    startActivity(ourIntent);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;

            case R.id.cmAnagrams:

                Class ourClass2;
                try {
                    ourClass2 = Class.forName("com.matt1.thenewboston.Anagram");
                    Intent ourIntent2 = new Intent(ClueMenu.this, ourClass2);
                    startActivity(ourIntent2);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                break;

            case R.id.cmMapclues:

                Class ourClass3;
                try {
                    ourClass3 = Class.forName("com.matt1.thenewboston.MapClue");
                    Intent ourIntent3 = new Intent(ClueMenu.this, ourClass3);
                    startActivity(ourIntent3);
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }



                break;



        }





    }





}