package com.matt1.thenewboston;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Matthew on 1/2/2015.
 */
public class RuneAnim extends Activity{
    private AnimatorSet animatorSet;
    private AnimatorSet animatorSet1;
    private ObjectAnimator waveOneAnimator;
    private ObjectAnimator waveTwoAnimator;
    private ObjectAnimator waveThreeAnimator;
    private ObjectAnimator waveFourAnimator;
    private ImageView air, water, earth, fire;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.runeanims);
//********* hangout ************
        air = (ImageView) findViewById(R.id.air);
        water = (ImageView) findViewById(R.id.water);
        earth = (ImageView) findViewById(R.id.earth);
        fire = (ImageView) findViewById(R.id.fire);


        Typeface myTypeface = Typeface.createFromAsset(getAssets(), "fonts/runescape_uf.ttf");


        //     hangoutTvOne.setTypeface(myTypeface);
        //     hangoutTvTwo.setTypeface(myTypeface);

        //     hangoutTvThree.setTypeface(myTypeface);


        waveAnimation();


    }


    public void waveAnimation() {
        PropertyValuesHolder tvOne_Y = PropertyValuesHolder.ofFloat(air.TRANSLATION_Y, -200.0f);
        PropertyValuesHolder tvOne_X = PropertyValuesHolder.ofFloat(air.TRANSLATION_X, 0);
        waveOneAnimator = ObjectAnimator.ofPropertyValuesHolder(air, tvOne_X, tvOne_Y);
        waveOneAnimator.setRepeatCount(-1);
        waveOneAnimator.setRepeatMode(ValueAnimator.REVERSE);
        waveOneAnimator.setDuration(300);
        waveOneAnimator.start();

        PropertyValuesHolder tvTwo_Y = PropertyValuesHolder.ofFloat(water.TRANSLATION_Y, -200.0f);
        PropertyValuesHolder tvTwo_X = PropertyValuesHolder.ofFloat(water.TRANSLATION_X, 0);
        waveTwoAnimator = ObjectAnimator.ofPropertyValuesHolder(water, tvTwo_X, tvTwo_Y);
        waveTwoAnimator.setRepeatCount(-1);
        waveTwoAnimator.setRepeatMode(ValueAnimator.REVERSE);
        waveTwoAnimator.setDuration(300);
        waveTwoAnimator.setStartDelay(100);
        waveTwoAnimator.start();

        PropertyValuesHolder tvThree_Y = PropertyValuesHolder.ofFloat(earth.TRANSLATION_Y, -200.0f);
        PropertyValuesHolder tvThree_X = PropertyValuesHolder.ofFloat(earth.TRANSLATION_X, 0);
        //PropertyValuesHolder tvThree_Z = PropertyValuesHolder.ofFloat(earth.TRANSLATION_X, 250);
        //PropertyValuesHolder tvThree_ZZ = PropertyValuesHolder.ofFloat(earth.TRANSLATION_Y, 100);




        waveThreeAnimator = ObjectAnimator.ofPropertyValuesHolder(earth, tvThree_X, tvThree_Y);

        waveThreeAnimator.setRepeatCount(-1);
        waveThreeAnimator.setRepeatMode(ValueAnimator.REVERSE);
        waveThreeAnimator.setDuration(300);
        waveThreeAnimator.setStartDelay(200);
        waveThreeAnimator.start();


        PropertyValuesHolder tvFour_Y = PropertyValuesHolder.ofFloat(fire.TRANSLATION_Y, -200.0f);
        PropertyValuesHolder tvFour_X = PropertyValuesHolder.ofFloat(fire.TRANSLATION_X, 0);
        waveFourAnimator = ObjectAnimator.ofPropertyValuesHolder(fire, tvFour_X, tvFour_Y);
        waveFourAnimator.setRepeatCount(-1);
        waveFourAnimator.setRepeatMode(ValueAnimator.REVERSE);
        waveFourAnimator.setDuration(300);
        waveFourAnimator.setStartDelay(300);
        waveFourAnimator.start();

    }


}