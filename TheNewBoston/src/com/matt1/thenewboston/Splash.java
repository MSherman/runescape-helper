package com.matt1.thenewboston;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class Splash extends ActionBarActivity {

	MediaPlayer ourSound;
    private AnimationDrawable myAnimationDrawable1;
    private ImageView runes;
    TextView tips, title;
    String[] tiplist = new String[10];
    private PendingIntent pendingIntent;
    private AlarmManager manager;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);

        runes = (ImageView) findViewById(R.id.incrementingBoxView);

        runes.setVisibility(View.VISIBLE);
        incrementalHorizontalLoading();

        Typeface myTypeface = Typeface.createFromAsset(getAssets(), "fonts/runescape_uf.ttf");

        tiplist[0] = "Entering and leaving a portal at Castle Wars will restore your health and prayer.";
        tiplist[1] = "Type \"::toggleroof\" in chat without quotes to remove roofs, allowing you to click inside banks a lot easier. ";
        tiplist[2] = "Rune boots give the same strength bonus as climbing boots, so if you're training with protection prayers on, there's no reason to risk an extra 50k.";
        tiplist[3] = "Void Knight bottoms give a better magic defense bonus than black d'hide chaps.";
        tiplist[4] = "Left-clicking on the text that asks you \"what you want to make\" when smithing cannon balls automatically selects your entire inventory so you don't have to \"select x\" and type in a number.";
        tiplist[5] = "Holding Ctrl while clicking toggles your run on automatically.";
        tiplist[6] = "You can sell your cat(s) to West Ardougne civilians for 100 death runes. This isn't bad money for a noob, considering growing a cat takes minimal attention.";
        tiplist[7] = "Save your dueling rings and games necklaces when they have one charge left. You can alch them for the same amount as when they were fully charged, which helps to pay for their cost.";
        tiplist[8] = "Using Hawk Eye instead of Eagle Eye when training range is much more cost efficient as it drains twice as slowly and gives only 2/3 less bonuses.";
        tiplist[9] = "Broad arrows (55 slayer) are very cost-efficient ammo (they have a ranged strength between mithril and adamant arrows). Buy a few hundred per world (at any slayer master) and then hop.";






        Intent alarmIntent = new Intent(this, AlarmReciever.class);
        pendingIntent = PendingIntent.getBroadcast(this, 0, alarmIntent, 0);



        title  = (TextView) findViewById(R.id.title);




        tips = (TextView) findViewById(R.id.tipsview);


        tips.setTypeface(myTypeface);
        title.setTypeface(myTypeface);

        tips.setText("Tip: " + tiplist[new Random().nextInt(10)]);

		ourSound = MediaPlayer.create(Splash.this, R.raw.rsnoise);
		ourSound.start();   //plays the sound after setting the contentview
		
		
		
		Thread timer = new Thread(){
			public void run(){
				try{
					sleep(5000);
					
				} catch (InterruptedException e){
					e.printStackTrace();
				}finally{
					Intent openStartingPoint = new Intent("com.matt1.thenewboston.MAINMENU");
					startActivity(openStartingPoint);
				}
			}
		};
		timer.start();
	}
    public void incrementalHorizontalLoading() {
        myAnimationDrawable1 = (AnimationDrawable) runes.getDrawable();
        myAnimationDrawable1.start();
    }


    public void startAlarm(View view){

        manager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        int interval = 10000;


        manager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), interval, pendingIntent);
        Toast.makeText(this, "Alarm Set", Toast.LENGTH_SHORT).show();

    }




	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		ourSound.release();
		finish();
	}

	
}
