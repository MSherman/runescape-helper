package com.matt1.thenewboston;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

/**
 * Created by Matthew on 1/1/2015.
 */
public class QuestGuides extends Activity implements AdapterView.OnItemSelectedListener{

    String url;
    WebView questView;
    Spinner quests;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.questguide);

  //      questView = (WebView) findViewById(R.id.webView);

  //      questView.setWebViewClient(new WebViewClient());

   //     questView.loadUrl("http://2007.runescape.wikia.com/wiki/contact!");


        quests = (Spinner) findViewById(R.id.spinner2);
// Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.Quest_List, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        quests.setAdapter(adapter);


        quests.setOnItemSelectedListener(this);



    }



    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


        if(!quests.getSelectedItem().toString().equalsIgnoreCase("Choose a Quest")) {
            questView = (WebView) findViewById(R.id.webView);
            url = "http://2007.runescape.wikia.com/wiki/" + quests.getSelectedItem().toString().replace(" ", "_");


            questView.clearCache(true);
            questView.clearHistory();

            questView.setWebViewClient(new WebViewClient());
            questView.loadUrl(url);
            //  questView.setWebViewClient(new WebViewClient());
        }
    }

    @Override
    public void onBackPressed() {
        if (questView.canGoBack()) {
            questView.goBack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


}