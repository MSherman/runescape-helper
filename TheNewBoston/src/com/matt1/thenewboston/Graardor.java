package com.matt1.thenewboston;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Graardor extends Activity implements OnClickListener {


    ImageButton[] x =  new ImageButton[27];
    int kc = 0;
    int kc2 = 0;
    int counter = 1;

    GridLayout grid;

    String filename = "graardordrops";
    Button viewLog, closeLog, dropSum;
    LinearLayout one;
    TableLayout ll;
    int i= 0;

    TextView kcount, lastDrop, bossname, logboss, logKc;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.droppage);
        File myDir = getFilesDir();
        File file = new File(myDir + "/text/", filename);

        Typeface myTypeface = Typeface.createFromAsset(getAssets(), "fonts/runescape_uf.ttf");


        kcount = (TextView) findViewById(R.id.killCountNum);
        logKc = (TextView) findViewById(R.id.logKC);


        logboss = (TextView) findViewById(R.id.logBoss);



        bossname = (TextView) findViewById(R.id.bossName);
        bossname.setText("General Graardor");


        logboss.setText(bossname.getText().toString());

        bossname.setTypeface(myTypeface);

        grid = (GridLayout) findViewById(R.id.grid);

        ll = (TableLayout) findViewById(R.id.tables);

        viewLog = (Button) findViewById(R.id.viewlogs);
        closeLog = (Button) findViewById(R.id.closelog);

        lastDrop = (TextView) findViewById(R.id.lastDropText);

        dropSum = (Button) findViewById(R.id.dropSummary);


        one = (LinearLayout) findViewById(R.id.fucker);



        LinearLayout.LayoutParams p1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 100);

        LinearLayout brow1 = new LinearLayout(this);
        brow1.setOrientation(LinearLayout.HORIZONTAL);
        brow1.setWeightSum(4);
        brow1.setLayoutParams(p1);

        LinearLayout brow2 = new LinearLayout(this);
        brow2.setOrientation(LinearLayout.HORIZONTAL);
        brow2.setWeightSum(4);
        brow2.setLayoutParams(p1);

        LinearLayout brow3 = new LinearLayout(this);
        brow3.setOrientation(LinearLayout.HORIZONTAL);
        brow3.setWeightSum(4);
        brow3.setLayoutParams(p1);

        LinearLayout brow4 = new LinearLayout(this);
        brow4.setOrientation(LinearLayout.HORIZONTAL);
        brow4.setWeightSum(4);
        brow4.setLayoutParams(p1);

        LinearLayout brow5 = new LinearLayout(this);
        brow5.setOrientation(LinearLayout.HORIZONTAL);
        brow5.setWeightSum(4);
        brow5.setLayoutParams(p1);

        LinearLayout brow6 = new LinearLayout(this);
        brow6.setOrientation(LinearLayout.HORIZONTAL);
        brow6.setWeightSum(4);
        brow6.setLayoutParams(p1);

        LinearLayout brow7 = new LinearLayout(this);
        brow7.setOrientation(LinearLayout.HORIZONTAL);
        brow7.setWeightSum(4);
        brow7.setLayoutParams(p1);

        LinearLayout brow8 = new LinearLayout(this);
        brow8.setOrientation(LinearLayout.HORIZONTAL);
        brow8.setWeightSum(4);
        brow8.setLayoutParams(p1);





        LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(100, 100);
        p.weight = 1;
        p.gravity = Gravity.CENTER;
        //  p.height = 600;








        x[0] = new ImageButton(this);
        x[0].setImageResource(R.drawable.bandos_hilt);
        x[0].setTag(new DropButton("Bandos hilt", 1));
        x[0].setLayoutParams(p);
        x[0].setBackgroundResource(android.R.color.transparent);
        x[0].setScaleType(ImageButton.ScaleType.FIT_CENTER);




        x[1] = new ImageButton(this);
        x[1].setImageResource(R.drawable.godsword_shard1);
        x[1].setTag(new DropButton("Godsword shard 1", 1));
        x[1].setLayoutParams(p);
        x[1].setBackgroundResource(android.R.color.transparent);
        x[1].setScaleType(ImageButton.ScaleType.FIT_CENTER);


        x[2] = new ImageButton(this);
        x[2].setImageResource(R.drawable.godsword_shard2);
        x[2].setTag(new DropButton("Godsword shard 2", 1));
        x[2].setLayoutParams(p);
        x[2].setBackgroundResource(android.R.color.transparent);
        x[2].setScaleType(ImageButton.ScaleType.FIT_CENTER);

        x[3] = new ImageButton(this);
        x[3].setImageResource(R.drawable.godsword_shard3);
        x[3].setTag(new DropButton("Godsword shard 3", 1));
        x[3].setLayoutParams(p);
        x[3].setBackgroundResource(android.R.color.transparent);
        x[3].setScaleType(ImageButton.ScaleType.FIT_CENTER);

        brow1.addView(x[0]);
        brow1.addView(x[1]);
        brow1.addView(x[2]);
        brow1.addView(x[3]);




        x[4] = new ImageButton(this);
        x[4].setImageResource(R.drawable.bandos_chestplate);
        x[4].setTag(new DropButton("Bandos chestplate", 1));
        x[4].setLayoutParams(p);
        x[4].setBackgroundResource(android.R.color.transparent);
        x[4].setScaleType(ImageButton.ScaleType.FIT_CENTER);


        x[5] = new ImageButton(this);
        x[5].setImageResource(R.drawable.bandos_tassets);
        x[5].setTag(new DropButton("Bandos tassets", 1));
        x[5].setLayoutParams(p);
        x[5].setBackgroundResource(android.R.color.transparent);
        x[5].setScaleType(ImageButton.ScaleType.FIT_CENTER);

        x[6] = new ImageButton(this);
        x[6].setImageResource(R.drawable.bandos_boots);
        x[6].setTag(new DropButton("Bandos boots", 1));
        x[6].setLayoutParams(p);
        x[6].setBackgroundResource(android.R.color.transparent);
        x[6].setScaleType(ImageButton.ScaleType.FIT_CENTER);

        x[7] = new ImageButton(this);
        x[7].setImageResource(R.drawable.rune_2h_sword);
        x[7].setTag(new DropButton("Rune 2h sword", 1));
        x[7].setLayoutParams(p);
        x[7].setBackgroundResource(android.R.color.transparent);
        x[7].setScaleType(ImageButton.ScaleType.FIT_CENTER);

        brow2.addView(x[4]);
        brow2.addView(x[5]);
        brow2.addView(x[6]);
        brow2.addView(x[7]);




        x[8] = new ImageButton(this);
        x[8].setImageResource(R.drawable.rune_pickaxe);
        x[8].setTag(new DropButton("Rune pickaxe", 1));
        x[8].setLayoutParams(p);
        x[8].setBackgroundResource(android.R.color.transparent);
        x[8].setScaleType(ImageButton.ScaleType.FIT_CENTER);

        x[9] = new ImageButton(this);
        x[9].setImageResource(R.drawable.rune_platebody);
        x[9].setTag(new DropButton("Rune platebody", 1));
        x[9].setLayoutParams(p);
        x[9].setBackgroundResource(android.R.color.transparent);
        x[9].setScaleType(ImageButton.ScaleType.FIT_CENTER);

        x[10] = new ImageButton(this);
        x[10].setImageResource(R.drawable.rune_longsword);
        x[10].setTag(new DropButton("Rune longsword", 1));
        x[10].setLayoutParams(p);
        x[10].setBackgroundResource(android.R.color.transparent);
        x[10].setScaleType(ImageButton.ScaleType.FIT_CENTER);

        x[11] = new ImageButton(this);
        x[11].setImageResource(R.drawable.rune_battleaxe);
        x[11].setTag(new DropButton("Rune battleaxe", 1));
        x[11].setLayoutParams(p);
        x[11].setBackgroundResource(android.R.color.transparent);
        x[11].setScaleType(ImageButton.ScaleType.FIT_CENTER);


        brow3.addView(x[8]);
        brow3.addView(x[9]);
        brow3.addView(x[10]);
        brow3.addView(x[11]);


        x[12] = new ImageButton(this);
        x[12].setImageResource(R.drawable.dragon_med_helm);
        x[12].setTag(new DropButton("Dragon med helm", 1));
        x[12].setLayoutParams(p);
        x[12].setBackgroundResource(android.R.color.transparent);
        x[12].setScaleType(ImageButton.ScaleType.FIT_CENTER);

        x[13] = new ImageButton(this);
        x[13].setImageResource(R.drawable.rune_sword);
        x[13].setTag(new DropButton("Rune sword", 1));
        x[13].setLayoutParams(p);
        x[13].setBackgroundResource(android.R.color.transparent);
        x[13].setScaleType(ImageButton.ScaleType.FIT_CENTER);

        x[14] = new ImageButton(this);
        x[14].setImageResource(R.drawable.coins);
        x[14].setTag(new DropButton("Coins", 20000));
        x[14].setLayoutParams(p);
        x[14].setBackgroundResource(android.R.color.transparent);
        x[14].setScaleType(ImageButton.ScaleType.FIT_CENTER);

        x[15] = new ImageButton(this);
        x[15].setImageResource(R.drawable.snap_seed);
        x[15].setTag(new DropButton("Snapdragon seed", 2));
        x[15].setLayoutParams(p);
        x[15].setBackgroundResource(android.R.color.transparent);
        x[15].setScaleType(ImageButton.ScaleType.FIT_CENTER);


        brow4.addView(x[12]);
        brow4.addView(x[13]);
        brow4.addView(x[14]);
        brow4.addView(x[15]);


        x[16] = new ImageButton(this);
        x[16].setImageResource(R.drawable.nature_rune);
        x[16].setTag(new DropButton("Nature rune", 65));
        x[16].setLayoutParams(p);
        x[16].setBackgroundResource(android.R.color.transparent);
        x[16].setScaleType(ImageButton.ScaleType.FIT_CENTER);


        x[17] = new ImageButton(this);
        x[17].setImageResource(R.drawable.adamantite_ore);
        x[17].setTag(new DropButton("Adamantite ore", 17));
        x[17].setLayoutParams(p);
        x[17].setBackgroundResource(android.R.color.transparent);
        x[17].setScaleType(ImageButton.ScaleType.FIT_CENTER);

        x[18] = new ImageButton(this);
        x[18].setImageResource(R.drawable.coal);
        x[18].setTag(new DropButton("Coal", 117));
        x[18].setLayoutParams(p);
        x[18].setBackgroundResource(android.R.color.transparent);
        x[18].setScaleType(ImageButton.ScaleType.FIT_CENTER);

        x[19] = new ImageButton(this);
        x[19].setImageResource(R.drawable.magic_logs);
        x[19].setTag(new DropButton("Magic logs", 17));
        x[19].setLayoutParams(p);
        x[19].setBackgroundResource(android.R.color.transparent);
        x[19].setScaleType(ImageButton.ScaleType.FIT_CENTER);



        brow5.addView(x[16]);
        brow5.addView(x[17]);
        brow5.addView(x[18]);
        brow5.addView(x[19]);

        x[20] = new ImageButton(this);
        x[20].setImageResource(R.drawable.super_restore);
        x[20].setTag(new DropButton("Super restore", 3));
        x[20].setLayoutParams(p);
        x[20].setBackgroundResource(android.R.color.transparent);
        x[20].setScaleType(ImageButton.ScaleType.FIT_CENTER);

        x[21] = new ImageButton(this);
        x[21].setImageResource(R.drawable.grimy_snapdragon);
        x[21].setTag(new DropButton("Grimy snapdragon", 3));
        x[21].setLayoutParams(p);
        x[21].setBackgroundResource(android.R.color.transparent);
        x[21].setScaleType(ImageButton.ScaleType.FIT_CENTER);

        x[22] = new ImageButton(this);
        x[22].setImageResource(R.drawable.long_bone);
        x[22].setTag(new DropButton("Long bone", 1));
        x[22].setLayoutParams(p);
        x[22].setBackgroundResource(android.R.color.transparent);
        x[22].setScaleType(ImageButton.ScaleType.FIT_CENTER);

        x[23] = new ImageButton(this);
        x[23].setImageResource(R.drawable.curved_bone);
        x[23].setTag(new DropButton("Curved bone", 1));
        x[23].setLayoutParams(p);
        x[23].setBackgroundResource(android.R.color.transparent);
        x[23].setScaleType(ImageButton.ScaleType.FIT_CENTER);





        brow6.addView(x[20]);
        brow6.addView(x[21]);
        brow6.addView(x[22]);
        brow6.addView(x[23]);


        x[24] = new ImageButton(this);
        x[24].setImageResource(R.drawable.clue_scroll);
        x[24].setTag(new DropButton("Clue scroll", 1));
        x[24].setLayoutParams(p);
        x[24].setBackgroundResource(android.R.color.transparent);
        x[24].setScaleType(ImageButton.ScaleType.FIT_CENTER);

        x[25] = new ImageButton(this);
        x[25].setImageResource(R.drawable.pet_graardor);
        x[25].setTag(new DropButton("Pet general graaror", 3));
        x[25].setLayoutParams(p);
        x[25].setBackgroundResource(android.R.color.transparent);
        x[25].setScaleType(ImageButton.ScaleType.FIT_CENTER);

        x[26] = new ImageButton(this);
        x[26].setImageResource(R.drawable.raredroptable);
        x[26].setTag(new DropButton("Rare drop table", 1));
        x[26].setLayoutParams(p);
        x[26].setBackgroundResource(android.R.color.transparent);
        x[26].setScaleType(ImageButton.ScaleType.FIT_CENTER);







        brow7.addView(x[24]);
        brow7.addView(x[25]);
        brow7.addView(x[26]);






        grid.addView(brow1);
        grid.addView(brow2);
        grid.addView(brow3);
        grid.addView(brow4);
        grid.addView(brow5);
        grid.addView(brow6);
        grid.addView(brow7);




        x[0].setOnClickListener(this);
        x[1].setOnClickListener(this);
        x[2].setOnClickListener(this);
        x[3].setOnClickListener(this);
        x[4].setOnClickListener(this);
        x[5].setOnClickListener(this);
        x[6].setOnClickListener(this);
        x[7].setOnClickListener(this);
        x[8].setOnClickListener(this);
        x[9].setOnClickListener(this);
        x[10].setOnClickListener(this);
        x[11].setOnClickListener(this);
        x[12].setOnClickListener(this);
        x[13].setOnClickListener(this);
        x[14].setOnClickListener(this);
        x[15].setOnClickListener(this);
        x[16].setOnClickListener(this);
        x[17].setOnClickListener(this);
        x[18].setOnClickListener(this);
        x[19].setOnClickListener(this);
        x[20].setOnClickListener(this);
        x[21].setOnClickListener(this);
        x[22].setOnClickListener(this);
        x[23].setOnClickListener(this);
        x[24].setOnClickListener(this);
        x[25].setOnClickListener(this);
        x[26].setOnClickListener(this);

        fillLog();

        viewLog.setTypeface(myTypeface);
        closeLog.setTypeface(myTypeface);
        dropSum.setTypeface(myTypeface);


        viewLog.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                one.setVisibility(View.VISIBLE);

                ll.removeAllViews();
                kc2=0;
                counter=1;
                i=0;
                fillLog();



            }
        });

        closeLog.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                one.setVisibility(View.GONE);
                i=0;

                counter=1;
            }
        });

        dropSum.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                one.setVisibility(View.VISIBLE);
                i=0;
                kc2=0;
                ll.removeAllViews();
                fillDropSum();



            }
        });

    }

    public void fillDropSum() {


        File myDir = getFilesDir();

        try {
            // File secondInputFile = new File(myDir + "/text/", filename);
            //   InputStream secondInputStream = new BufferedInputStream(new FileInputStream(secondInputFile));
            //  BufferedReader r = new BufferedReader(new InputStreamReader(secondInputStream));
            //   StringBuilder total = new StringBuilder();
            String line;
            String item= "";
            int count2 = 0;
            int quant2 = 0;
            //      Log.e("does this work", "please");

            for(int d = 0; d<x.length; d++) {

                File secondInputFile = new File(myDir + "/text/", filename);
                InputStream secondInputStream = new BufferedInputStream(new FileInputStream(secondInputFile));
                BufferedReader r = new BufferedReader(new InputStreamReader(secondInputStream));
                StringBuilder total = new StringBuilder();

                while ((line = r.readLine()) != null) {


                    if(d==0)
                        kc2++;



                    DropButton bloo = (DropButton) x[d].getTag();


                    quant2 = bloo.getQuantity();
                    item = bloo.getItem();
                    if(line.contains(item))
                        count2++;

                    //     total.append(line);
                    //    TableRow row = new TableRow(this);
                    //   TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
                    //     row.setLayoutParams(lp);


                    //   TextView text = new TextView(this);
                    //   text.setText("" + counter + "." + line);

                    //   row.addView(text);
                    //   ll.addView(row, i);
                    //   i++;
                    //   counter++;

                    //           Log.e("appeneded", "nice job");
                }
//            Log.e("this is the Line", line);


                int totalquant = quant2*count2;


                TextView text = new TextView(this);
                text.setText(item);

                TextView text2 = new TextView(this);
                text2.setText("" + count2);

                TextView text3 = new TextView(this);
                text3.setText("" + totalquant);

                TableRow row = new TableRow(this);
                TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
                row.setLayoutParams(lp);

                row.addView(text);
                row.addView(text2);
                row.addView(text3);


                kcount.setText("" + kc2);
                logKc.setText("" + kc2);



                if(d==0) {

                    TextView itemtext = new TextView(this);
                    itemtext.setText("Item");
                    itemtext.setTextSize(20);

                    TextView quanttext = new TextView(this);
                    quanttext.setText("Quantity");
                    quanttext.setTextSize(20);

                    TextView totaltext = new TextView(this);
                    totaltext.setText("Total");
                    totaltext.setTextSize(20);

                    TableRow row2 = new TableRow(this);
                    TableRow.LayoutParams lpp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
                    row.setLayoutParams(lpp);

                    row2.addView(itemtext);
                    row2.addView(quanttext);
                    row2.addView(totaltext);

                    ll.addView(row2, d);


                }



                ll.addView(row, d+1);
                count2=0;






                r.close();
                secondInputStream.close();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void fillLog() {



        File myDir = getFilesDir();

        try {

            String line;
            String item= "";
            int count2 = 0;




            File secondInputFile = new File(myDir + "/text/", filename);
            InputStream secondInputStream = new BufferedInputStream(new FileInputStream(secondInputFile));
            BufferedReader r = new BufferedReader(new InputStreamReader(secondInputStream));


            while ((line = r.readLine()) != null) {


                TableRow row = new TableRow(this);
                TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
                row.setLayoutParams(lp);
                kc2++;

                TextView text = new TextView(this);
                text.setText("" + counter + "." + line);

                row.addView(text);
                ll.addView(row, i);
                i++;
                counter++;

            }


            kcount.setText("" + kc2);
            logKc.setText("" + kc2);
            r.close();
            secondInputStream.close();


        } catch (Exception e) {
            e.printStackTrace();
        }


    }



    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub


        DropButton blah = (DropButton) v.getTag();

        String item = blah.getItem();
        int quant = blah.getQuantity();


        v.startAnimation(AnimationUtils.loadAnimation(this, R.anim.clickanim));
        kc2++;

        kcount.setText("" + kc2);
        logKc.setText("" + kc2);
        lastDrop.setText(item);



        File myDir = getFilesDir();






        BufferedWriter writer = null;
        int num = 1;
        String newline = System.getProperty("line.separator");
        try {
            //create a temporary file
            // String WritertimeLog = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
            File logFile = new File(myDir + "/text/", filename);

            // This will output the full path where the file will be written to...
            // System.out.println(logFile.getCanonicalPath());

            if (logFile.getParentFile().mkdirs())
                logFile.createNewFile();



            writer = new BufferedWriter(new FileWriter(logFile, true));
            writer.append("" + quant + "\t");
            writer.append(item);
            writer.write(newline);
            Log.e("PLeASE0", "Work");


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Close the writer regardless of what happens...
                writer.close();
            } catch (Exception e) {
            }
        }


    }




}
