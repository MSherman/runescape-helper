package com.matt1.thenewboston;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


import android.app.Activity;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class DropLog extends Activity implements OnClickListener {

   // ImageButton d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28;
       ImageButton[] x =  new ImageButton[28];
        int kc = 0;
    int counter = 1;

    String filename = "rexdrops";
    Button viewLog, closeLog;
    LinearLayout one;
    TableLayout ll;
    int i= 0;

    TextView kcount, lastDrop;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dagannoth_rex);
        File myDir = getFilesDir();
        File file = new File(myDir + "/text/", filename);

        kcount = (TextView) findViewById(R.id.killCountNum);


        ll = (TableLayout) findViewById(R.id.tables);

        viewLog = (Button) findViewById(R.id.viewlogs);
        closeLog = (Button) findViewById(R.id.closelog);
        lastDrop = (TextView) findViewById(R.id.lastDropText);


        one = (LinearLayout) findViewById(R.id.fucker);


        x[0] = (ImageButton) findViewById(R.id.drop1);
        x[0].setImageResource(R.drawable.berserker_ring);
        x[0].setTag(new DropButton("Berserker ring", 1));

        x[1] = (ImageButton) findViewById(R.id.drop2);
        x[1].setImageResource(R.drawable.warrior_ring);
        x[1].setTag(new DropButton("Warrior ring", 1));

        x[2] = (ImageButton) findViewById(R.id.drop3);
        x[2].setImageResource(R.drawable.dragon_axe);
        x[2].setTag(new DropButton("Dragon axe", 1));

        x[3] = (ImageButton) findViewById(R.id.drop4);
        x[3].setImageResource(R.drawable.ring_of_life);
        x[3].setTag(new DropButton("Ring of life", 1));

        x[4] = (ImageButton) findViewById(R.id.drop5);
        x[4].setImageResource(R.drawable.fremennik_blade);
        x[4].setTag(new DropButton("Fremennik blade", 1));

        x[5] = (ImageButton) findViewById(R.id.drop6);
        x[5].setImageResource(R.drawable.mithril_warhammer);
        x[5].setTag(new DropButton("Mithril warhammer", 1));

        x[6] = (ImageButton) findViewById(R.id.drop7);
        x[6].setImageResource(R.drawable.adamant_axe);
        x[6].setTag(new DropButton("Adamant axe", 1));

        x[7] = (ImageButton) findViewById(R.id.drop8);
        x[7].setImageResource(R.drawable.mithril_2h_sword);
        x[7].setTag(new DropButton("Mithril 2h sword", 1));

        x[8] = (ImageButton) findViewById(R.id.drop9);
        x[8].setImageResource(R.drawable.mithril_pickaxe);
        x[8].setTag(new DropButton("Mithril Pickaxe", 1));

        x[9] = (ImageButton) findViewById(R.id.drop10);
        x[9].setImageResource(R.drawable.rune_axe);
        x[9].setTag(new DropButton("Rune axe", 1));

        x[10] = (ImageButton) findViewById(R.id.drop11);
        x[10].setImageResource(R.drawable.rune_battleaxe);
        x[10].setTag(new DropButton("Rune battleaxe", 1));

        x[11] = (ImageButton) findViewById(R.id.drop12);
        x[11].setImageResource(R.drawable.steel_kiteshield);
        x[11].setTag(new DropButton("Steel kiteshield", 1));

        x[12] = (ImageButton) findViewById(R.id.drop13);

        x[12].setImageResource(R.drawable.steel_platebody);
        x[12].setTag(new DropButton("Steel platebody", 1));

        x[13] = (ImageButton) findViewById(R.id.drop14);
        x[13].setImageResource(R.drawable.adamant_platebody);
        x[13].setTag(new DropButton("Adamant platebody", 1));

        x[14] = (ImageButton) findViewById(R.id.drop15);
        x[14].setImageResource(R.drawable.fremennik_shield);
        x[14].setTag(new DropButton("Fremennik shield", 1));

        x[15] = (ImageButton) findViewById(R.id.drop16);
        x[15].setImageResource(R.drawable.fremennik_helm);
        x[15].setTag(new DropButton("Fremennik helm", 1));

        x[16] = (ImageButton) findViewById(R.id.drop17);
        x[16].setImageResource(R.drawable.rock_shell_plate);
        x[16].setTag(new DropButton("Rock-shell plate", 1));

        x[17] = (ImageButton) findViewById(R.id.drop18);
        x[17].setImageResource(R.drawable.rock_shell_legs);
        x[17].setTag(new DropButton("Rock-shell legs", 1));

        x[18] = (ImageButton) findViewById(R.id.drop19);
        x[18].setImageResource(R.drawable.potiondrop);
        x[18].setTag(new DropButton("Any Potions", 1));

        x[19] = (ImageButton) findViewById(R.id.drop20);
        x[19].setImageResource(R.drawable.addy_bar);
        x[19].setTag(new DropButton("Adamant bar", 1));

        x[20] = (ImageButton) findViewById(R.id.drop21);
        x[20].setImageResource(R.drawable.mithril_ore);
        x[20].setTag(new DropButton("Mithril ore", 25));

        x[21] = (ImageButton) findViewById(R.id.drop22);
        x[21].setImageResource(R.drawable.coal);
        x[21].setTag(new DropButton("Coal", 100));

        x[22] = (ImageButton) findViewById(R.id.drop23);
        x[22].setImageResource(R.drawable.iron_ore);
        x[22].setTag(new DropButton("Iron", 150));

        x[23] = (ImageButton) findViewById(R.id.drop24);
        x[23].setImageResource(R.drawable.steel_bar);
        x[23].setTag(new DropButton("Steel bar", 22));

        x[24] = (ImageButton) findViewById(R.id.drop25);
        x[24].setImageResource(R.drawable.bass);
        x[24].setTag(new DropButton("Bass", 5));

        x[25] = (ImageButton) findViewById(R.id.drop26);
        x[25].setImageResource(R.drawable.swordfish);
        x[25].setTag(new DropButton("Swordfish", 5));

        x[26] = (ImageButton) findViewById(R.id.drop27);
        x[26].setImageResource(R.drawable.shark);
        x[26].setTag(new DropButton("Shark", 5));

        x[27] = (ImageButton) findViewById(R.id.drop28);
        x[27].setImageResource(R.drawable.berserker_ring);
        x[27].setTag(new DropButton("RDT", 1));


        x[0].setOnClickListener(this);
        x[1].setOnClickListener(this);
        x[2].setOnClickListener(this);
        x[3].setOnClickListener(this);
        x[4].setOnClickListener(this);
        x[5].setOnClickListener(this);
        x[6].setOnClickListener(this);
        x[7].setOnClickListener(this);
        x[8].setOnClickListener(this);
        x[9].setOnClickListener(this);
        x[10].setOnClickListener(this);
        x[11].setOnClickListener(this);
        x[12].setOnClickListener(this);
        x[13].setOnClickListener(this);
        x[14].setOnClickListener(this);
        x[15].setOnClickListener(this);
        x[16].setOnClickListener(this);
        x[17].setOnClickListener(this);
        x[18].setOnClickListener(this);
        x[19].setOnClickListener(this);
        x[20].setOnClickListener(this);
        x[21].setOnClickListener(this);
        x[22].setOnClickListener(this);
        x[23].setOnClickListener(this);
        x[24].setOnClickListener(this);
        x[25].setOnClickListener(this);
        x[26].setOnClickListener(this);
        x[27].setOnClickListener(this);

        viewLog.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                one.setVisibility(View.VISIBLE);

              ll.removeAllViews();
            addRow();



            }
        });

        closeLog.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                one.setVisibility(View.GONE);
                i=0;
                counter=1;
            }
        });

    }

    public void addRow() {

        String filename = "rexdrops";

        File myDir = getFilesDir();

        try {
           // File secondInputFile = new File(myDir + "/text/", filename);
         //   InputStream secondInputStream = new BufferedInputStream(new FileInputStream(secondInputFile));
          //  BufferedReader r = new BufferedReader(new InputStreamReader(secondInputStream));
         //   StringBuilder total = new StringBuilder();
            String line;
            String item= "";
            int count2 = 0;
            //      Log.e("does this work", "please");

            for(int d = 0; d<x.length; d++) {

                File secondInputFile = new File(myDir + "/text/", filename);
                InputStream secondInputStream = new BufferedInputStream(new FileInputStream(secondInputFile));
                BufferedReader r = new BufferedReader(new InputStreamReader(secondInputStream));
                StringBuilder total = new StringBuilder();

                while ((line = r.readLine()) != null) {



                    DropButton bloo = (DropButton) x[d].getTag();

                    item = bloo.getItem();
                    if(line.contains(item))
                        count2++;

               //     total.append(line);
                //    TableRow row = new TableRow(this);
                //   TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
               //     row.setLayoutParams(lp);


                 //   TextView text = new TextView(this);
                 //   text.setText("" + counter + "." + line);

                 //   row.addView(text);
                 //   ll.addView(row, i);
                 //   i++;
                 //   counter++;

                    //           Log.e("appeneded", "nice job");
                }
//            Log.e("this is the Line", line);





                TextView text = new TextView(this);
                text.setText(item);

                TextView text2 = new TextView(this);
                text2.setText("" + count2);

                TextView text3 = new TextView(this);
                text3.setText("" + count2);

                TableRow row = new TableRow(this);
                TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
                row.setLayoutParams(lp);

                row.addView(text);
                row.addView(text2);
                row.addView(text3);


                if(d==0) {

                    TextView itemtext = new TextView(this);
                    itemtext.setText("Item");
                    itemtext.setTextSize(20);

                    TextView quanttext = new TextView(this);
                    quanttext.setText("Quantity");
                    quanttext.setTextSize(20);

                    TextView totaltext = new TextView(this);
                    totaltext.setText("Total");
                    totaltext.setTextSize(20);

                    TableRow row2 = new TableRow(this);
                    TableRow.LayoutParams lpp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
                    row.setLayoutParams(lpp);

                    row2.addView(itemtext);
                    row2.addView(quanttext);
                    row2.addView(totaltext);

                    ll.addView(row2, d);


                }



                ll.addView(row, d+1);
                count2=0;






                r.close();
                secondInputStream.close();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }





    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub


        DropButton blah = (DropButton) v.getTag();

        String item = blah.getItem();
        int quant = blah.getQuantity();


        v.startAnimation(AnimationUtils.loadAnimation(this, R.anim.clickanim));
        kc++;

        kcount.setText("" + kc);
        lastDrop.setText(item);



        File myDir = getFilesDir();

        String filename = "rexdrops";





        BufferedWriter writer = null;
        int num = 1;
        String newline = System.getProperty("line.separator");
        try {
            //create a temporary file
            // String WritertimeLog = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
            File logFile = new File(myDir + "/text/", filename);

            // This will output the full path where the file will be written to...
            // System.out.println(logFile.getCanonicalPath());

            if (logFile.getParentFile().mkdirs())
                logFile.createNewFile();



            writer = new BufferedWriter(new FileWriter(logFile, true));
            writer.append("" + quant + "\t");
            writer.append(item);
            writer.write(newline);
            Log.e("PLeASE0", "Work");


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Close the writer regardless of what happens...
                writer.close();
            } catch (Exception e) {
            }
        }


    }




}
