package com.matt1.thenewboston;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Vetion extends Activity implements OnClickListener {

    // ImageButton d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28;
    ImageButton[] x =  new ImageButton[29];
    int kc = 0;
    int counter = 1;

    GridLayout grid;

    String filename = "vetiondrops";
    Button viewLog, closeLog, dropSum;
    LinearLayout one;
    TableLayout ll;
    int i= 0;

    TextView kcount, lastDrop, bossname, logboss;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.droppage);
        File myDir = getFilesDir();
        File file = new File(myDir + "/text/", filename);

        Typeface myTypeface = Typeface.createFromAsset(getAssets(), "fonts/runescape_uf.ttf");


        kcount = (TextView) findViewById(R.id.killCountNum);


        logboss = (TextView) findViewById(R.id.logBoss);



        bossname = (TextView) findViewById(R.id.bossName);
        bossname.setText("Vetion ");


        logboss.setText(bossname.getText().toString());

        bossname.setTypeface(myTypeface);

        grid = (GridLayout) findViewById(R.id.grid);

        ll = (TableLayout) findViewById(R.id.tables);

        viewLog = (Button) findViewById(R.id.viewlogs);
        closeLog = (Button) findViewById(R.id.closelog);

        lastDrop = (TextView) findViewById(R.id.lastDropText);

        dropSum = (Button) findViewById(R.id.dropSummary);


        one = (LinearLayout) findViewById(R.id.fucker);



        LinearLayout.LayoutParams p1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 100);

        LinearLayout brow1 = new LinearLayout(this);
        brow1.setOrientation(LinearLayout.HORIZONTAL);
        brow1.setWeightSum(4);
        brow1.setLayoutParams(p1);

        LinearLayout brow2 = new LinearLayout(this);
        brow2.setOrientation(LinearLayout.HORIZONTAL);
        brow2.setWeightSum(4);
        brow2.setLayoutParams(p1);

        LinearLayout brow3 = new LinearLayout(this);
        brow3.setOrientation(LinearLayout.HORIZONTAL);
        brow3.setWeightSum(4);
        brow3.setLayoutParams(p1);

        LinearLayout brow4 = new LinearLayout(this);
        brow4.setOrientation(LinearLayout.HORIZONTAL);
        brow4.setWeightSum(4);
        brow4.setLayoutParams(p1);

        LinearLayout brow5 = new LinearLayout(this);
        brow5.setOrientation(LinearLayout.HORIZONTAL);
        brow5.setWeightSum(4);
        brow5.setLayoutParams(p1);

        LinearLayout brow6 = new LinearLayout(this);
        brow6.setOrientation(LinearLayout.HORIZONTAL);
        brow6.setWeightSum(4);
        brow6.setLayoutParams(p1);

        LinearLayout brow7 = new LinearLayout(this);
        brow7.setOrientation(LinearLayout.HORIZONTAL);
        brow7.setWeightSum(4);
        brow7.setLayoutParams(p1);

        LinearLayout brow8 = new LinearLayout(this);
        brow8.setOrientation(LinearLayout.HORIZONTAL);
        brow8.setWeightSum(4);
        brow8.setLayoutParams(p1);





        LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(100, 100);
        p.weight = 1;
        p.gravity = Gravity.CENTER;
        //  p.height = 600;








        x[0] = new ImageButton(this);
        x[0].setImageResource(R.drawable.dragon_pickaxe);
        x[0].setTag(new DropButton("Dragon Pickaxe", 1));
        x[0].setLayoutParams(p);
        x[0].setBackgroundResource(android.R.color.transparent);
        x[0].setScaleType(ImageButton.ScaleType.FIT_CENTER);




        x[1] = new ImageButton(this);
        x[1].setImageResource(R.drawable.ring_of_the_gods);
        x[1].setTag(new DropButton("Ring of the gods", 1));
        x[1].setLayoutParams(p);
        x[1].setBackgroundResource(android.R.color.transparent);
        x[1].setScaleType(ImageButton.ScaleType.FIT_CENTER);


        x[2] = new ImageButton(this);
        x[2].setImageResource(R.drawable.dragon_2h_sword);
        x[2].setTag(new DropButton("Dragon 2h Sword", 1));
        x[2].setLayoutParams(p);
        x[2].setBackgroundResource(android.R.color.transparent);
        x[2].setScaleType(ImageButton.ScaleType.FIT_CENTER);

        x[3] = new ImageButton(this);
        x[3].setImageResource(R.drawable.rune_pickaxe);
        x[3].setTag(new DropButton("Rune pickaxe", 1));
        x[3].setLayoutParams(p);
        x[3].setBackgroundResource(android.R.color.transparent);
        x[3].setScaleType(ImageButton.ScaleType.FIT_CENTER);

        brow1.addView(x[0]);
        brow1.addView(x[1]);
        brow1.addView(x[2]);
        brow1.addView(x[3]);




        x[4] = new ImageButton(this);
        x[4].setImageResource(R.drawable.ancient_staff);
        x[4].setTag(new DropButton("Ancient staff", 1));
        x[4].setLayoutParams(p);
        x[4].setBackgroundResource(android.R.color.transparent);
        x[4].setScaleType(ImageButton.ScaleType.FIT_CENTER);


        x[5] = new ImageButton(this);
        x[5].setImageResource(R.drawable.chaos_rune);
        x[5].setTag(new DropButton("Chaos rune", 400));
        x[5].setLayoutParams(p);
        x[5].setBackgroundResource(android.R.color.transparent);
        x[5].setScaleType(ImageButton.ScaleType.FIT_CENTER);

        x[6] = new ImageButton(this);
        x[6].setImageResource(R.drawable.death_rune);
        x[6].setTag(new DropButton("Death rune", 300));
        x[6].setLayoutParams(p);
        x[6].setBackgroundResource(android.R.color.transparent);
        x[6].setScaleType(ImageButton.ScaleType.FIT_CENTER);

        x[7] = new ImageButton(this);
        x[7].setImageResource(R.drawable.blood_rune);
        x[7].setTag(new DropButton("Blood rune", 200));
        x[7].setLayoutParams(p);
        x[7].setBackgroundResource(android.R.color.transparent);
        x[7].setScaleType(ImageButton.ScaleType.FIT_CENTER);

        brow2.addView(x[4]);
        brow2.addView(x[5]);
        brow2.addView(x[6]);
        brow2.addView(x[7]);




        x[8] = new ImageButton(this);
        x[8].setImageResource(R.drawable.rune_2h_sword);
        x[8].setTag(new DropButton("Rune 2h sword", 1));
        x[8].setLayoutParams(p);
        x[8].setBackgroundResource(android.R.color.transparent);
        x[8].setScaleType(ImageButton.ScaleType.FIT_CENTER);

        x[9] = new ImageButton(this);
        x[9].setImageResource(R.drawable.dragon_longsword);
        x[9].setTag(new DropButton("Dragon longsword", 1));
        x[9].setLayoutParams(p);
        x[9].setBackgroundResource(android.R.color.transparent);
        x[9].setScaleType(ImageButton.ScaleType.FIT_CENTER);

        x[10] = new ImageButton(this);
        x[10].setImageResource(R.drawable.ogre_coffin_key);
        x[10].setTag(new DropButton("Ogre coffin key", 10));
        x[10].setLayoutParams(p);
        x[10].setBackgroundResource(android.R.color.transparent);
        x[10].setScaleType(ImageButton.ScaleType.FIT_CENTER);

        x[11] = new ImageButton(this);
        x[11].setImageResource(R.drawable.dragon_bones);
        x[11].setTag(new DropButton("Dragon bones", 100));
        x[11].setLayoutParams(p);
        x[11].setBackgroundResource(android.R.color.transparent);
        x[11].setScaleType(ImageButton.ScaleType.FIT_CENTER);


        brow3.addView(x[8]);
        brow3.addView(x[9]);
        brow3.addView(x[10]);
        brow3.addView(x[11]);


        x[12] = new ImageButton(this);
        x[12].setImageResource(R.drawable.soft_clay);
        x[12].setTag(new DropButton("Soft clay", 50));
        x[12].setLayoutParams(p);
        x[12].setBackgroundResource(android.R.color.transparent);
        x[12].setScaleType(ImageButton.ScaleType.FIT_CENTER);

        x[13] = new ImageButton(this);
        x[13].setImageResource(R.drawable.green_dragonhide);
        x[13].setTag(new DropButton("Green dragonhide", 100));
        x[13].setLayoutParams(p);
        x[13].setBackgroundResource(android.R.color.transparent);
        x[13].setScaleType(ImageButton.ScaleType.FIT_CENTER);

        x[14] = new ImageButton(this);
        x[14].setImageResource(R.drawable.coins);
        x[14].setTag(new DropButton("Coins", 16000));
        x[14].setLayoutParams(p);
        x[14].setBackgroundResource(android.R.color.transparent);
        x[14].setScaleType(ImageButton.ScaleType.FIT_CENTER);

        x[15] = new ImageButton(this);
        x[15].setImageResource(R.drawable.dark_crab_restores);
        x[15].setTag(new DropButton("Dark Crab/Restores", 1));
        x[15].setLayoutParams(p);
        x[15].setBackgroundResource(android.R.color.transparent);
        x[15].setScaleType(ImageButton.ScaleType.FIT_CENTER);


        brow4.addView(x[12]);
        brow4.addView(x[13]);
        brow4.addView(x[14]);
        brow4.addView(x[15]);


        x[16] = new ImageButton(this);
        x[16].setImageResource(R.drawable.uncut_ruby_diamond);
        x[16].setTag(new DropButton("Uncut ruby/diamond", 1));
        x[16].setLayoutParams(p);
        x[16].setBackgroundResource(android.R.color.transparent);
        x[16].setScaleType(ImageButton.ScaleType.FIT_CENTER);


        x[17] = new ImageButton(this);
        x[17].setImageResource(R.drawable.gold_ore);
        x[17].setTag(new DropButton("Gold ore", 300));
        x[17].setLayoutParams(p);
        x[17].setBackgroundResource(android.R.color.transparent);
        x[17].setScaleType(ImageButton.ScaleType.FIT_CENTER);

        x[18] = new ImageButton(this);
        x[18].setImageResource(R.drawable.ranarr_weed);
        x[18].setTag(new DropButton("Ranarr weed", 100));
        x[18].setLayoutParams(p);
        x[18].setBackgroundResource(android.R.color.transparent);
        x[18].setScaleType(ImageButton.ScaleType.FIT_CENTER);

        x[19] = new ImageButton(this);
        x[19].setImageResource(R.drawable.sanfew_serum);
        x[19].setTag(new DropButton("Sanfew Serum(4)", 10));
        x[19].setLayoutParams(p);
        x[19].setBackgroundResource(android.R.color.transparent);
        x[19].setScaleType(ImageButton.ScaleType.FIT_CENTER);



        brow5.addView(x[16]);
        brow5.addView(x[17]);
        brow5.addView(x[18]);
        brow5.addView(x[19]);

        x[20] = new ImageButton(this);
        x[20].setImageResource(R.drawable.mort_myre_fungus);
        x[20].setTag(new DropButton("Mort myre fungus", 200));
        x[20].setLayoutParams(p);
        x[20].setBackgroundResource(android.R.color.transparent);
        x[20].setScaleType(ImageButton.ScaleType.FIT_CENTER);

        x[21] = new ImageButton(this);
        x[21].setImageResource(R.drawable.magic_logs);
        x[21].setTag(new DropButton("Magic Logs", 100));
        x[21].setLayoutParams(p);
        x[21].setBackgroundResource(android.R.color.transparent);
        x[21].setScaleType(ImageButton.ScaleType.FIT_CENTER);

        x[22] = new ImageButton(this);
        x[22].setImageResource(R.drawable.uncut_dragonstone);
        x[22].setTag(new DropButton("Uncut dragonstone", 1));
        x[22].setLayoutParams(p);
        x[22].setBackgroundResource(android.R.color.transparent);
        x[22].setScaleType(ImageButton.ScaleType.FIT_CENTER);

        x[23] = new ImageButton(this);
        x[23].setImageResource(R.drawable.dark_fishing_bait);
        x[23].setTag(new DropButton("Dark fishing bait", 375));
        x[23].setLayoutParams(p);
        x[23].setBackgroundResource(android.R.color.transparent);
        x[23].setScaleType(ImageButton.ScaleType.FIT_CENTER);





        brow6.addView(x[20]);
        brow6.addView(x[21]);
        brow6.addView(x[22]);
        brow6.addView(x[23]);

        x[24] = new ImageButton(this);
        x[24].setImageResource(R.drawable.clue_scroll);
        x[24].setTag(new DropButton("Clue scroll", 1));
        x[24].setLayoutParams(p);
        x[24].setBackgroundResource(android.R.color.transparent);
        x[24].setScaleType(ImageButton.ScaleType.FIT_CENTER);

        x[25] = new ImageButton(this);
        x[25].setImageResource(R.drawable.limpwurt_root);
        x[25].setTag(new DropButton("Limpwurt root", 50));
        x[25].setLayoutParams(p);
        x[25].setBackgroundResource(android.R.color.transparent);
        x[25].setScaleType(ImageButton.ScaleType.FIT_CENTER);

        x[26] = new ImageButton(this);
        x[26].setImageResource(R.drawable.molten_glass);
        x[26].setTag(new DropButton("Molten glass", 50));
        x[26].setLayoutParams(p);
        x[26].setBackgroundResource(android.R.color.transparent);
        x[26].setScaleType(ImageButton.ScaleType.FIT_CENTER);

        x[27] = new ImageButton(this);
        x[27].setImageResource(R.drawable.mysterious_emblem);
        x[27].setTag(new DropButton("Mysterious emblem", 1));
        x[27].setLayoutParams(p);
        x[27].setBackgroundResource(android.R.color.transparent);
        x[27].setScaleType(ImageButton.ScaleType.FIT_CENTER);

        brow7.addView(x[24]);
        brow7.addView(x[25]);
        brow7.addView(x[26]);
        brow7.addView(x[27]);


        x[28] = new ImageButton(this);
        x[28].setImageResource(R.drawable.raredroptable);
        x[28].setTag(new DropButton("Rare Drop Table", 1));
        x[28].setLayoutParams(p);
        x[28].setBackgroundResource(android.R.color.transparent);
        x[28].setScaleType(ImageButton.ScaleType.FIT_CENTER);



        brow8.addView(x[28]);





        grid.addView(brow1);
        grid.addView(brow2);
        grid.addView(brow3);
        grid.addView(brow4);
        grid.addView(brow5);
        grid.addView(brow6);
        grid.addView(brow7);
        grid.addView(brow8);




        x[0].setOnClickListener(this);
        x[1].setOnClickListener(this);
        x[2].setOnClickListener(this);
        x[3].setOnClickListener(this);
        x[4].setOnClickListener(this);
        x[5].setOnClickListener(this);
        x[6].setOnClickListener(this);
        x[7].setOnClickListener(this);
        x[8].setOnClickListener(this);
        x[9].setOnClickListener(this);
        x[10].setOnClickListener(this);
        x[11].setOnClickListener(this);
        x[12].setOnClickListener(this);
        x[13].setOnClickListener(this);
        x[14].setOnClickListener(this);
        x[15].setOnClickListener(this);
        x[16].setOnClickListener(this);
        x[17].setOnClickListener(this);
        x[18].setOnClickListener(this);
        x[19].setOnClickListener(this);
        x[20].setOnClickListener(this);
        x[21].setOnClickListener(this);
        x[22].setOnClickListener(this);
        x[23].setOnClickListener(this);
        x[24].setOnClickListener(this);
        x[25].setOnClickListener(this);
        x[26].setOnClickListener(this);
        x[27].setOnClickListener(this);
        x[28].setOnClickListener(this);


        viewLog.setTypeface(myTypeface);
        closeLog.setTypeface(myTypeface);
        dropSum.setTypeface(myTypeface);


        viewLog.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                one.setVisibility(View.VISIBLE);

                ll.removeAllViews();
                fillLog();



            }
        });

        closeLog.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                one.setVisibility(View.GONE);
                i=0;
                counter=1;
            }
        });

        dropSum.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                one.setVisibility(View.VISIBLE);

                ll.removeAllViews();
                fillDropSum();



            }
        });

    }

    public void fillDropSum() {


        File myDir = getFilesDir();

        try {
            // File secondInputFile = new File(myDir + "/text/", filename);
            //   InputStream secondInputStream = new BufferedInputStream(new FileInputStream(secondInputFile));
            //  BufferedReader r = new BufferedReader(new InputStreamReader(secondInputStream));
            //   StringBuilder total = new StringBuilder();
            String line;
            String item= "";
            int count2 = 0;
            int quant2 = 0;
            //      Log.e("does this work", "please");

            for(int d = 0; d<x.length; d++) {

                File secondInputFile = new File(myDir + "/text/", filename);
                InputStream secondInputStream = new BufferedInputStream(new FileInputStream(secondInputFile));
                BufferedReader r = new BufferedReader(new InputStreamReader(secondInputStream));
                StringBuilder total = new StringBuilder();

                while ((line = r.readLine()) != null) {



                    DropButton bloo = (DropButton) x[d].getTag();


                    quant2 = bloo.getQuantity();
                    item = bloo.getItem();
                    if(line.contains(item))
                        count2++;

                    //     total.append(line);
                    //    TableRow row = new TableRow(this);
                    //   TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
                    //     row.setLayoutParams(lp);


                    //   TextView text = new TextView(this);
                    //   text.setText("" + counter + "." + line);

                    //   row.addView(text);
                    //   ll.addView(row, i);
                    //   i++;
                    //   counter++;

                    //           Log.e("appeneded", "nice job");
                }
//            Log.e("this is the Line", line);


                int totalquant = quant2*count2;


                TextView text = new TextView(this);
                text.setText(item);

                TextView text2 = new TextView(this);
                text2.setText("" + count2);

                TextView text3 = new TextView(this);
                text3.setText("" + totalquant);

                TableRow row = new TableRow(this);
                TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
                row.setLayoutParams(lp);

                row.addView(text);
                row.addView(text2);
                row.addView(text3);


                if(d==0) {

                    TextView itemtext = new TextView(this);
                    itemtext.setText("Item");
                    itemtext.setTextSize(20);

                    TextView quanttext = new TextView(this);
                    quanttext.setText("Quantity");
                    quanttext.setTextSize(20);

                    TextView totaltext = new TextView(this);
                    totaltext.setText("Total");
                    totaltext.setTextSize(20);

                    TableRow row2 = new TableRow(this);
                    TableRow.LayoutParams lpp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
                    row.setLayoutParams(lpp);

                    row2.addView(itemtext);
                    row2.addView(quanttext);
                    row2.addView(totaltext);

                    ll.addView(row2, d);


                }



                ll.addView(row, d+1);
                count2=0;






                r.close();
                secondInputStream.close();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void fillLog() {



        File myDir = getFilesDir();

        try {

            String line;
            String item= "";
            int count2 = 0;




            File secondInputFile = new File(myDir + "/text/", filename);
            InputStream secondInputStream = new BufferedInputStream(new FileInputStream(secondInputFile));
            BufferedReader r = new BufferedReader(new InputStreamReader(secondInputStream));


            while ((line = r.readLine()) != null) {


                TableRow row = new TableRow(this);
                TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
                row.setLayoutParams(lp);


                TextView text = new TextView(this);
                text.setText("" + counter + "." + line);

                row.addView(text);
                ll.addView(row, i);
                i++;
                counter++;

            }


            r.close();
            secondInputStream.close();


        } catch (Exception e) {
            e.printStackTrace();
        }


    }



    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub


        DropButton blah = (DropButton) v.getTag();

        String item = blah.getItem();
        int quant = blah.getQuantity();


        v.startAnimation(AnimationUtils.loadAnimation(this, R.anim.clickanim));
        kc++;

        kcount.setText("" + kc);
        lastDrop.setText(item);



        File myDir = getFilesDir();






        BufferedWriter writer = null;
        int num = 1;
        String newline = System.getProperty("line.separator");
        try {
            //create a temporary file
            // String WritertimeLog = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
            File logFile = new File(myDir + "/text/", filename);

            // This will output the full path where the file will be written to...
            // System.out.println(logFile.getCanonicalPath());

            if (logFile.getParentFile().mkdirs())
                logFile.createNewFile();



            writer = new BufferedWriter(new FileWriter(logFile, true));
            writer.append("" + quant + "\t");
            writer.append(item);
            writer.write(newline);
            Log.e("PLeASE0", "Work");


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Close the writer regardless of what happens...
                writer.close();
            } catch (Exception e) {
            }
        }


    }




}
