package com.matt1.thenewboston;

import android.app.Activity;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.animation.ScaleAnimation;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ScrollView;
import android.widget.Toast;

/**
 * Created by Matthew on 1/1/2015.
 */
public class Tweets extends Activity {

    private float mScale = 1f;
    public ScaleGestureDetector mScaleDetector;
    GestureDetector gestureDetector;
    WebView dis;
    ScrollView layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.jagextwit);

        dis = (WebView) findViewById(R.id.tweets);


        dis.getSettings().setDomStorageEnabled(true);
        dis.getSettings().setJavaScriptEnabled(true);

        dis.setWebViewClient(new WebViewClient() {

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {

            }
        });

        String summary = "            <a class=\"twitter-timeline\"  href=\"https://twitter.com/search?q=from%3Ajagex_ghost%20OR%20from%3Aoldschoolrs%20OR%20from%3AJagexArchie%20OR%20from%3AJagexRonan%20OR%20from%3AJagexWeath%20OR%20from%3AJagexJohnC%20OR%20from%3AJagexMatK%20OR%20from%3AJagexReach%20OR%20from%3AJagexAsh\" data-widget-id=\"552743197303386112\">Tweets about from:jagex_ghost OR from:oldschoolrs OR from:JagexArchie OR from:JagexRonan OR from:JagexWeath OR from:JagexJohnC OR from:JagexMatK OR from:JagexReach OR from:JagexAsh</a>\n" +
                "            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+\"://platform.twitter.com/widgets.js\";fjs.parentNode.insertBefore(js,fjs);}}(document,\"script\",\"twitter-wjs\");</script>\n" +
                "          ";

        dis.loadDataWithBaseURL("https://twitter.com", summary, "text/html", "UTF-8", null);




    }


}
