package com.matt1.thenewboston;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.view.View;
import android.view.inputmethod.InputMethodManager;



public class Hiscores extends Activity implements View.OnClickListener{

	public TextView skills[] = new TextView[24];
			
			
		//	{total, attack, defence, strength, hitpoints, ranged, prayer, magic,
	// cooking, woodcutting, fletching, fishing, firemaking, crafting, smithing, mining,
	//  herblore, agility, thieving, slayer, farming, runecraft, hunter, construction};
	String name;
	public EditText username;
	public Button search;
	String url =  "http://services.runescape.com/m=hiscore_oldschool/hiscorepersonal.ws?user1=";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.hiscore);
		search = (Button) findViewById(R.id.ibSearch);
	//	test = (TextView) findViewById(R.id.tvTest);
		username = (EditText) findViewById(R.id.etUsername);
		skills[0] = (TextView) findViewById(R.id.tvTotal);
		skills[1] = (TextView) findViewById(R.id.tvAttack);
		skills[2] = (TextView) findViewById(R.id.tvDefence);
		skills[3] = (TextView) findViewById(R.id.tvStrength);
		skills[4] = (TextView) findViewById(R.id.tvHitpoints);
		skills[5] = (TextView) findViewById(R.id.tvRanged);
		skills[6] = (TextView) findViewById(R.id.tvPrayer);
		skills[7] = (TextView) findViewById(R.id.tvMagic);
		skills[8] = (TextView) findViewById(R.id.tvCooking);
		skills[9] = (TextView) findViewById(R.id.tvWoodcutting);
		skills[10] = (TextView) findViewById(R.id.tvFletching);
		skills[11] = (TextView) findViewById(R.id.tvFishing);
		skills[12] = (TextView) findViewById(R.id.tvFiremaking);
		skills[13] = (TextView) findViewById(R.id.tvCrafting);
		skills[14] = (TextView) findViewById(R.id.tvSmithing);
		skills[15] = (TextView) findViewById(R.id.tvMining);
		skills[16] = (TextView) findViewById(R.id.tvHerblore);
		skills[17] = (TextView) findViewById(R.id.tvAgility);
		skills[18] = (TextView) findViewById(R.id.tvThieving);
		skills[19] = (TextView) findViewById(R.id.tvSlayer);
		skills[20] = (TextView) findViewById(R.id.tvFarming);
		skills[21] = (TextView) findViewById(R.id.tvRunecrafting);
		skills[22] = (TextView) findViewById(R.id.tvHunter);
		skills[23] = (TextView) findViewById(R.id.tvConstruction);
		search.setOnClickListener(this);
		
		
		
	}
	
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE); 

		inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                   InputMethodManager.HIDE_NOT_ALWAYS);
		name = username.getText().toString();
		url = "http://services.runescape.com/m=hiscore_oldschool/hiscorepersonal.ws?user1=" + name;
		new Title().execute();
		//test.setText(url);
		

		
	}
	
	

class Title extends AsyncTask<Void, Void, Void> {
    String title;
    Elements content;
    @Override
    protected Void doInBackground(Void... params) {
        try {
            // Connect to the web site
            Document document = Jsoup.connect(url).get();
            // Get the html document title
            content = document.getElementsByTag("td");
            //title = document.title();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    @Override
    protected void onPostExecute(Void result) {
        // Set title into TextView
        int x = 0;
        if(content.size() > 100){
        for(int i = 15; i <= 130; i=i+5){
        skills[x].setText(content.get(i).text());
        x++;
        }
        
        }
        else{
            for(int i = 15; i <= 130; i=i+5){
                skills[x].setText("" + 0);
                x++;
            }
        	
        }
    }
	
	
}
	
}

