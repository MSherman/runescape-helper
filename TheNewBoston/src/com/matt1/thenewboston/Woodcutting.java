package com.matt1.thenewboston;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.matt1.thenewboston.Hiscores.Title;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.PorterDuff.Mode;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import org.json.JSONArray;
import org.json.JSONObject;



public class Woodcutting extends Activity implements OnClickListener, AdapterView.OnItemSelectedListener {

    Button getStats, calculate;
    // TextView lvl, normal, oak, willow, maple, yew, magic;
    TextProgressBar progress;
    TextView lvl;
    EditText username, xp, targetLvl;

    double skillxp[] = { 25, 37.5, 67.5, 85, 100, 125, 175, 250 };
    TextView names[] = new TextView[8];
    int progressStatus = 0;
    String name2, name;
    String targets = "1";

    Spinner targ;

    String url = "http://services.runescape.com/m=hiscore_oldschool/index_lite.ws?player=";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wccalc);
        getStats = (Button) findViewById(R.id.bStats);
        calculate = (Button) findViewById(R.id.bCalculate);


        String filename = "mysecondfile";

        File myDir = getFilesDir();

        try {
            File secondInputFile = new File(myDir + "/text/", filename);
            InputStream secondInputStream = new BufferedInputStream(new FileInputStream(secondInputFile));
            BufferedReader r = new BufferedReader(new InputStreamReader(secondInputStream));
            StringBuilder total = new StringBuilder();
            String line;
            while ((line = r.readLine()) != null) {
                total.append(line);
            }
            r.close();
            secondInputStream.close();
            name2 = total.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }





        targ = (Spinner) findViewById(R.id.spinner3);
// Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence>

                adapter = ArrayAdapter.createFromResource(this,
                R.array.numbers_array, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        targ.setAdapter(adapter);


        targ.setOnItemSelectedListener(this);




        Typeface myTypeface = Typeface.createFromAsset(getAssets(), "fonts/runescape_uf.ttf");

        getStats.setTypeface(myTypeface);
        calculate.setTypeface(myTypeface);









        progress = (TextProgressBar) findViewById(R.id.pbSkill);
        lvl = (TextView) findViewById(R.id.tvLvl);
        names[0] = (TextView) findViewById(R.id.tvNormal);
        names[1] = (TextView) findViewById(R.id.tvOak);
        names[2] = (TextView) findViewById(R.id.tvWillow);
        names[3] = (TextView) findViewById(R.id.tvTeak);
        names[4] = (TextView) findViewById(R.id.tvMaple);
        names[5] = (TextView) findViewById(R.id.tvMahogany);
        names[6] = (TextView) findViewById(R.id.tvYew);
        names[7] = (TextView) findViewById(R.id.tvMagic);

        username = (EditText) findViewById(R.id.etUsername);
        xp = (EditText) findViewById(R.id.etXP);
    //    targetLvl = (EditText) findViewById(R.id.etTarget);

        username.setInputType(android.text.InputType.TYPE_CLASS_TEXT | android.text.InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

        username.setText(name2);

        getStats.setOnClickListener(this);
        calculate.setOnClickListener(this);

    }

    public void calculateXP() {

        //checking if the current xp or target lvl box are empty before trying to use them
        if (xp.getText().toString().length() > 0) {
            int xpLeft = Integer.parseInt(targets);
            int currentXP = Integer.parseInt(xp.getText().toString()
                    .replace(",", ""));
            double y = calcXP(xpLeft);

            double x = (currentXP / y) * 100.0;
            int percent = (int) x;
            xpLeft = calcXP(xpLeft) - currentXP;

            int actions = 0;
            for (int i = 0; i < names.length; i++) {
                actions = (int) (xpLeft / skillxp[i] + 1);
                names[i].setText("" + actions);

            }

            progress.setProgress(percent);
            if (percent < 101)
                progress.setText("" + percent + "%");
            else
                progress.setText("" + 100 + "%");
        }
    }

    public int calcXP(int L) {

        int a = 0;

        for (int x = 1; x < L; x++) {

            a += Math.floor(x + 300 * Math.pow(2.0, (x / 7.0)));

        }

        return (int) Math.floor(a / 4);

    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub

        switch (v.getId()) {
            case R.id.bStats:

                InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

                inputManager.hideSoftInputFromWindow(getCurrentFocus()
                        .getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                name = username.getText().toString();
                url = "http://services.runescape.com/m=hiscore_oldschool/index_lite.ws?player="
                        + name;
                new Title().execute();

                break;
            case R.id.bCalculate:

                InputMethodManager inputManager1 = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

                inputManager1.hideSoftInputFromWindow(getCurrentFocus()
                        .getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                progress.getProgressDrawable().setColorFilter(Color.GREEN,
                        Mode.MULTIPLY);
                calculateXP();

                break;

        }

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        targets = targ.getSelectedItem().toString();




    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    class Title extends AsyncTask<Void, Void, Void> {
        String title;
        String content;

        @Override
        protected Void doInBackground(Void... params) {
            try {
                // Connect to the web site
                Document document = Jsoup.connect(url).ignoreHttpErrors(true).get();
                // Get the html document title
                if(document.hasText())
                content = document.text();
                // title = document.title();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // Set title into TextView

            String delims = "[ ,]";




            if (content.length() > 22) {

                String[] stats = content.split(delims);
                if(stats.length>25)
                xp.setText(stats[29]);
                lvl.setText("Lvl:" + stats[28]);
                // after setting the xp an Lvl, it changes targetLvl to the
                // current level + 1

                int tl = Integer.parseInt(stats[28]) + 1;

                if(tl!=100)
                targ.setSelection(tl-1);
                else
                targ.setSelection(98);



                progress.getProgressDrawable().setColorFilter(Color.GREEN,
                        Mode.MULTIPLY);
                calculateXP();
            }
        }
    }

}
