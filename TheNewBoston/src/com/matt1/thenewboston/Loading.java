package com.matt1.thenewboston;

import android.app.Activity;
import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;


public class Loading extends Activity{

    private AnimationDrawable myAnimationDrawable1;
    private ImageView runes;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.runesloading);


        runes = (ImageView) findViewById(R.id.incrementingBoxView);

        runes.setVisibility(View.VISIBLE);
        incrementalHorizontalLoading();

    }



    public void incrementalHorizontalLoading() {
        myAnimationDrawable1 = (AnimationDrawable) runes.getDrawable();
        myAnimationDrawable1.start();
    }










}
