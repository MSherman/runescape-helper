package com.matt1.thenewboston;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.view.View;
import android.view.inputmethod.InputMethodManager;



public class Hiscores2 extends Activity implements View.OnClickListener{

    public TextView skillLvl[] = new TextView[24];
    public TextView skillRank[] = new TextView[24];
    public TextView skillXP[] = new TextView[24];
    public TextProgressBar progress[] =  new TextProgressBar[24];

    //	{total, attack, defence, strength, hitpoints, ranged, prayer, magic,
    // cooking, woodcutting, fletching, fishing, firemaking, crafting, smithing, mining,
    //  herblore, agility, thieving, slayer, farming, runecraft, hunter, construction};
    String name;
    public EditText username;
    public ImageButton search;
    String url =  "http://services.runescape.com/m=hiscore_oldschool/hiscorepersonal.ws?user1=";
    private AnimationDrawable myAnimationDrawable1;
    private ImageView runes;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        super.onCreate(savedInstanceState);
        setContentView(R.layout.loadpractice);

        String filename = "mysecondfile";

        File myDir = getFilesDir();

        try {
            File secondInputFile = new File(myDir + "/text/", filename);
            InputStream secondInputStream = new BufferedInputStream(new FileInputStream(secondInputFile));
            BufferedReader r = new BufferedReader(new InputStreamReader(secondInputStream));
            StringBuilder total = new StringBuilder();
            String line;
      //      Log.e("does this work", "please");
            while ((line = r.readLine()) != null) {
                total.append(line);
     //           Log.e("appeneded", "nice job");
            }
//            Log.e("this is the Line", line);
            r.close();
            secondInputStream.close();
            name = total.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }


        runes = (ImageView) findViewById(R.id.incrementingBoxView);

        runes.setVisibility(View.GONE);




        search =  (ImageButton) findViewById(R.id.ibSearch4);
        username = (EditText) findViewById(R.id.h2Username);

        username.setInputType(android.text.InputType.TYPE_CLASS_TEXT | android.text.InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

        search.setOnClickListener(this);


        username.setText(name);

        skillLvl[0] = (TextView) findViewById(R.id.totallvl);
        skillLvl[1] = (TextView) findViewById(R.id.attacklvl);
        skillLvl[2] = (TextView) findViewById(R.id.defencelvl);
        skillLvl[3] = (TextView) findViewById(R.id.strengthlvl);
        skillLvl[4] = (TextView) findViewById(R.id.hitpointslvl);
        skillLvl[5] = (TextView) findViewById(R.id.rangedlvl);
        skillLvl[6] = (TextView) findViewById(R.id.prayerlvl);
        skillLvl[7] = (TextView) findViewById(R.id.magiclvl);
        skillLvl[8] = (TextView) findViewById(R.id.cookinglvl);
        skillLvl[9] = (TextView) findViewById(R.id.woodcuttinglvl);
        skillLvl[10] = (TextView) findViewById(R.id.fletchinglvl);
        skillLvl[11] = (TextView) findViewById(R.id.fishinglvl);
        skillLvl[12] = (TextView) findViewById(R.id.firemakinglvl);
        skillLvl[13] = (TextView) findViewById(R.id.craftinglvl);
        skillLvl[14] = (TextView) findViewById(R.id.smithinglvl);
        skillLvl[15] = (TextView) findViewById(R.id.mininglvl);
        skillLvl[16] = (TextView) findViewById(R.id.herblorelvl);
        skillLvl[17] = (TextView) findViewById(R.id.agilitylvl);
        skillLvl[18] = (TextView) findViewById(R.id.thievinglvl);
        skillLvl[19] = (TextView) findViewById(R.id.slayerlvl);
        skillLvl[20] = (TextView) findViewById(R.id.farminglvl);
        skillLvl[21] = (TextView) findViewById(R.id.runecraftlvl);
        skillLvl[22] = (TextView) findViewById(R.id.hunterlvl);
        skillLvl[23] = (TextView) findViewById(R.id.constructionlvl);


        skillRank[0] = (TextView) findViewById(R.id.totalrank);
        skillRank[1] = (TextView) findViewById(R.id.attackrank);
        skillRank[2] = (TextView) findViewById(R.id.defencerank);
        skillRank[3] = (TextView) findViewById(R.id.strengthrank);
        skillRank[4] = (TextView) findViewById(R.id.hitpointsrank);
        skillRank[5] = (TextView) findViewById(R.id.rangedrank);
        skillRank[6] = (TextView) findViewById(R.id.prayerrank);
        skillRank[7] = (TextView) findViewById(R.id.magicrank);
        skillRank[8] = (TextView) findViewById(R.id.cookingrank);
        skillRank[9] = (TextView) findViewById(R.id.woodcuttingrank);
        skillRank[10] = (TextView) findViewById(R.id.fletchingrank);
        skillRank[11] = (TextView) findViewById(R.id.fishingrank);
        skillRank[12] = (TextView) findViewById(R.id.firemakingrank);
        skillRank[13] = (TextView) findViewById(R.id.craftingrank);
        skillRank[14] = (TextView) findViewById(R.id.smithingrank);
        skillRank[15] = (TextView) findViewById(R.id.miningrank);
        skillRank[16] = (TextView) findViewById(R.id.herblorerank);
        skillRank[17] = (TextView) findViewById(R.id.agilityrank);
        skillRank[18] = (TextView) findViewById(R.id.thievingrank);
        skillRank[19] = (TextView) findViewById(R.id.slayerrank);
        skillRank[20] = (TextView) findViewById(R.id.farmingrank);
        skillRank[21] = (TextView) findViewById(R.id.runecraftrank);
        skillRank[22] = (TextView) findViewById(R.id.hunterrank);
        skillRank[23] = (TextView) findViewById(R.id.constructionrank);


        skillXP[0] = (TextView) findViewById(R.id.totalxp);
        skillXP[1] = (TextView) findViewById(R.id.attackxp);
        skillXP[2] = (TextView) findViewById(R.id.defencexp);
        skillXP[3] = (TextView) findViewById(R.id.strengthxp);
        skillXP[4] = (TextView) findViewById(R.id.hitpointsxp);
        skillXP[5] = (TextView) findViewById(R.id.rangedxp);
        skillXP[6] = (TextView) findViewById(R.id.prayerxp);
        skillXP[7] = (TextView) findViewById(R.id.magicxp);
        skillXP[8] = (TextView) findViewById(R.id.cookingxp);
        skillXP[9] = (TextView) findViewById(R.id.woodcuttingxp);
        skillXP[10] = (TextView) findViewById(R.id.fletchingxp);
        skillXP[11] = (TextView) findViewById(R.id.fishingxp);
        skillXP[12] = (TextView) findViewById(R.id.firemakingxp);
        skillXP[13] = (TextView) findViewById(R.id.craftingxp);
        skillXP[14] = (TextView) findViewById(R.id.smithingxp);
        skillXP[15] = (TextView) findViewById(R.id.miningxp);
        skillXP[16] = (TextView) findViewById(R.id.herblorexp);
        skillXP[17] = (TextView) findViewById(R.id.agilityxp);
        skillXP[18] = (TextView) findViewById(R.id.thievingxp);
        skillXP[19] = (TextView) findViewById(R.id.slayerxp);
        skillXP[20] = (TextView) findViewById(R.id.farmingxp);
        skillXP[21] = (TextView) findViewById(R.id.runecraftxp);
        skillXP[22] = (TextView) findViewById(R.id.hunterxp);
        skillXP[23] = (TextView) findViewById(R.id.constructionxp);


        progress[0] = (TextProgressBar) findViewById(R.id.pbAttack);
        progress[1] = (TextProgressBar) findViewById(R.id.pbAttack);
        progress[2] = (TextProgressBar) findViewById(R.id.pbDefence);
        progress[3] = (TextProgressBar) findViewById(R.id.pbStrength);
        progress[4] = (TextProgressBar) findViewById(R.id.pbHitpoints);
        progress[5] = (TextProgressBar) findViewById(R.id.pbRanging);
        progress[6] = (TextProgressBar) findViewById(R.id.pbPrayer);
        progress[7] = (TextProgressBar) findViewById(R.id.pbMagic);
        progress[8] = (TextProgressBar) findViewById(R.id.pbCooking);
        progress[9] = (TextProgressBar) findViewById(R.id.pbWoodcutting);
        progress[10] = (TextProgressBar) findViewById(R.id.pbFletching);
        progress[11] = (TextProgressBar) findViewById(R.id.pbFishing);
        progress[12] = (TextProgressBar) findViewById(R.id.pbFiremaking);
        progress[13] = (TextProgressBar) findViewById(R.id.pbCrafting);
        progress[14] = (TextProgressBar) findViewById(R.id.pbSmithing);
        progress[15] = (TextProgressBar) findViewById(R.id.pbMining);
        progress[16] = (TextProgressBar) findViewById(R.id.pbHerblore);
        progress[17] = (TextProgressBar) findViewById(R.id.pbAgility);
        progress[18] = (TextProgressBar) findViewById(R.id.pbThieving);
        progress[19] = (TextProgressBar) findViewById(R.id.pbSlayer);
        progress[20] = (TextProgressBar) findViewById(R.id.pbFarming);
        progress[21] = (TextProgressBar) findViewById(R.id.pbRunecrafting);
        progress[22] = (TextProgressBar) findViewById(R.id.pbHunter);
        progress[23] = (TextProgressBar) findViewById(R.id.pbConstruction);








    }

    public void incrementalHorizontalLoading() {
        myAnimationDrawable1 = (AnimationDrawable) runes.getDrawable();
        myAnimationDrawable1.start();

    }


    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);


        name = username.getText().toString();

        url = "http://services.runescape.com/m=hiscore_oldschool/index_lite.ws?player=" + name;
        new Title().execute();

        runes.setVisibility(View.VISIBLE);
        incrementalHorizontalLoading();
        //test.setText(url);



    }



    class Title extends AsyncTask<Void, Void, Void> {
        String title;
        String content;

        @Override
        protected Void doInBackground(Void... params) {
            try {
                // Connect to the web site
                Document document = Jsoup.connect(url).ignoreHttpErrors(true).get();
                // Get the html document title
                if(document.hasText())
                    content = document.text();
                // title = document.title();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // Set title into TextView

            String delims = "[ ,]";


            int lvl = 1;
            int rank = 0;
            int xp = 2;
            int x = 0;

            int current = 0;
            double left = 0;
            double y = 0;
            int percent = 0;

            if (content.length() > 22) {

                String[] data = content.split(delims);
                DecimalFormat formatter = new DecimalFormat("#,###,###");
                String commas = formatter.format(100000);

                while(x<24)
                {



                    skillRank[x].setText(formatter.format(Integer.parseInt(data[rank])));
                    skillLvl[x].setText(data[lvl]);
                    skillXP[x].setText(formatter.format(Integer.parseInt(data[xp])));

                    if(x>0) {
                        current = calcXP(Integer.parseInt(data[lvl]));
                        left = calcXP(Integer.parseInt(data[lvl]) + 1);


                        left = left - current;
                        current = (Integer.parseInt(data[xp])) - current;


                        y = (current / left) * 100.0;
                        percent = (int) y;

                        progress[x].setProgress(percent);
                        if (percent < 101)
                            progress[x].setText("" + percent + "%");
                        else
                            progress[x].setText("" + 100 + "%");

                        progress[x].getProgressDrawable().setColorFilter(Color.GREEN,
                                PorterDuff.Mode.MULTIPLY);


                    }

                    lvl+=3;
                    rank+=3;
                    xp+=3;
                    x++;


                }

                //calculateXP();
            }

            runes.setVisibility(View.GONE);
            myAnimationDrawable1.stop();

        }
    }


    public int calcXP(int L) {

        int a = 0;

        for (int x = 1; x < L; x++) {

            a += Math.floor(x + 300 * Math.pow(2.0, (x / 7.0)));

        }

        return (int) Math.floor(a / 4);

    }
















    }



