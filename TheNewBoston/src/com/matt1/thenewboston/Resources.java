package com.matt1.thenewboston;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Matthew on 5/11/2015.
 */
public class Resources extends Activity implements View.OnClickListener {


    Button osrs, reddit, wikia, forums, polls;
    TextView resourcetitle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.resources);


        resourcetitle = (TextView) findViewById(R.id.resourcetitle);

        Typeface myTypeface = Typeface.createFromAsset(getAssets(), "fonts/runescape_uf.ttf");


        resourcetitle.setTypeface(myTypeface);


        osrs = (Button) findViewById(R.id.mmOSRS);
        reddit = (Button) findViewById(R.id.mmReddit);
        wikia = (Button) findViewById(R.id.mmWikia);
        forums = (Button) findViewById(R.id.mmForums);
        polls = (Button) findViewById(R.id.mmPolls);


        osrs.setOnClickListener(this);
        reddit.setOnClickListener(this);
        wikia.setOnClickListener(this);
        forums.setOnClickListener(this);
        polls.setOnClickListener(this);


    }


    @Override
    public void onClick(View v) {
        Intent browserIntent;

        switch (v.getId()) {
            case R.id.mmOSRS:
                browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://oldschool.runescape.com"));
                startActivity(browserIntent);

                break;


            case R.id.mmReddit:
                browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.reddit.com/r/2007scape"));
                startActivity(browserIntent);

                break;

            case R.id.mmWikia:
                browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://2007.runescape.wikia.com/wiki/2007scape_Wiki"));
                startActivity(browserIntent);

                break;

            case R.id.mmForums:
                browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://services.runescape.com/m=forum/forums.ws#group63"));
                startActivity(browserIntent);

                break;

            case R.id.mmPolls:
                browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://services.runescape.com/m=poll/oldschool/index.ws"));
                startActivity(browserIntent);

                break;
        }







    }

}
