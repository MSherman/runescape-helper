package com.matt1.thenewboston;

/**
 * Created by Matthew on 12/20/2014.
 */
public class AnagramBuilder {

    String question;
    String answer;
    String loc;
    String challenge;



    public AnagramBuilder(String anagram, String solvedAnagram, String location, String challenges) {
        question = anagram;
        answer = solvedAnagram;
        loc = location;
        challenge = challenges;

    }



    public String getAnswer()
    {


        return answer;



    }



    public String getLocation()
    {

        return loc;

    }





    public String getQuestion()
    {


        return question;



    }


    public String getChallenge()
    {

        return  challenge;



    }



}


